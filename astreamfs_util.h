/*
 * astreamfs and derivative work (1fichierfs) common code header
 *
 *
 * Copyright (C) 2018-2019  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/stat.h>
#include <syslog.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdatomic.h>
#include <inttypes.h>
#include <curl/curl.h>

/*
 * @brief options handling related macros
 *
 */
#define SZ_SHORT_ARG 2    /* Short arguments like -u have a size of 2        */
#define O_OFFSET 2        /* Offset from -- arg to -o arg is 2: double '-'   */
#define SZ_STD_ARG_TYPE 2 /* size of the standard argument type, ex. %i is 2 */

			  /* This macro generates both long arg and o-arg    */
#define FUSE_L_O_ARG(long_name, key) \
  FUSE_OPT_KEY(long_name, key), FUSE_OPT_KEY(long_name + O_OFFSET, key)

			  /* This macro generates all: short + the above     */
#define FUSE_ALL_ARG(short_name, long_name, key) \
  FUSE_OPT_KEY(short_name, key), FUSE_L_O_ARG(long_name, key)


/*
 * @brief HTTP Status codes we use
 *
 */
#define HTTP_OK			(200)
#define HTTP_PARTIAL_CONTENT	(206)
#define HTTP_BAD_REQUEST	(400)
#define HTTP_FORBIDDEN		(403)
#define HTTP_NOT_FOUND		(404)
#define HTTP_GONE		(410)
#define HTTP_TOO_MANY_REQUESTS	(429)
#define HTTP_MOVED_PERMANENTLY	(301)
#define HTTP_FOUND		(302)
#define HTTP_SEE_OTHER		(303)
#define HTTP_TEMPORARY_REDIRECT	(307)
#define HTTP_PERMANENT_REDIRECT (308)

/*
 * @brief Global parameters of the daemon.
 *
 */
struct params {
	struct stat mount_st;
	bool	foreground;
	bool	debug;
	bool	noatime;
	bool	noexec;
	bool	nosuid;
	unsigned int log_level;
	char *log_file;
	char *user_agent;
	char *filesystem_name;
	char *filesystem_subtype;
	char *ca_file;
};
extern struct params params;

/*
 * @brief the async reader's structure.
 */
struct reader {
	_Atomic (struct node *) rcv;
	struct node *prv;
	STREAM_STRUCT *s;
	off_t pos;
	off_t start;
	off_t end;
	unsigned long last_rq_id;
	unsigned int n_rq;
	sem_t sem_go;
	bool  idle;
	bool  streaming;
	CURL *curl;
	pthread_t thread;
};

#define MAX_READERS 4
extern struct reader readers[MAX_READERS];

/*
 * @brief log utility.
 *
 * The log utility printf either on terminal, on a file or to syslog depending
 * on the situation.
 * 
 * We have here global external variables, macro and function definition.
 *
 * The use is with the macro lprintf(LOG_xxx, ...)
 * LOG_xxx is a log level as defined in syslog.h
 * The rest are the same paramaters as printf.
 */

extern const char log_prefix[]; /* This needs to be defined along the include */
extern bool init_done;
extern FILE *log_fh;
extern struct timespec main_start;

#define MAX_LEVEL 7

extern void lprintf(int level, const char *pFormat, ... );


/*
 * @brief Utility macro to simplify error handling on curl_easy_setopt and
 *        curl_easy_getinfo
 */
#define CURL_EASY_SETOPT(c,o,a,fmt) \
	do { CURLcode res = curl_easy_setopt(c,o,a); \
	     if (CURLE_OK != res) \
	       lprintf( LOG_CRIT, \
			"%s at %d, err %d: curl_easy_setopt(c, " #o ", " fmt ")\n", \
			__FILE__, __LINE__, res, a);\
	   } while(0)

#define CURL_EASY_GETINFO(c,o,p) \
	do { CURLcode res = curl_easy_getinfo(c,o,p); \
	     if (CURLE_OK != res) \
		lprintf( LOG_CRIT, \
			"%s at %d, err %d: curl_easy_getinfo(c, " #o ",p)\n", \
			__FILE__, __LINE__, res );\
	   } while(0)


/*
 * @brief Memory allocation utility.
 *
 * Simplifies handling potential error on memory allocation.
 */
extern _Atomic unsigned long long mem_alloced;
extern _Atomic unsigned long long mem_freed  ;
extern _Atomic unsigned long long n_alloc    ;
extern _Atomic unsigned long long n_free     ;

extern void *fs_alloc(size_t size);
extern void *fs_calloc(size_t nmemb, size_t size);
extern void *fs_realloc(void *ptr, size_t size);
extern void  fs_free(void *p);
extern char *fs_strdup(const char *s);

/*
 * @brief Mutex lock-unlock utility.
 *
 * Simplifies handling and logging of mutex lock/unlock.
 */
extern void   lock(pthread_mutex_t *mutex, bool *locked);
extern void unlock(pthread_mutex_t *mutex, bool *locked);

/*
 * @brief Contention logging and counting (add) and get.
 */
extern void spin_add(unsigned long spin, const char* fct_name);
extern void spin_get(unsigned long *contentions, unsigned long *max_spins);

/*
 * @brief Lock free (producers)/wait free (consumer) message queue.
 */
struct node
{
	struct node *next;
	char  *buf;
	off_t  offset;
	size_t size;
	int    written;
	sem_t  sem_done;
};
extern void msg_push(_Atomic (struct node *) *head, struct node  *work);
extern void  msg_pop(_Atomic (struct node *) *head, struct node **work);


/*
 * @brief Expands arguments from the command line
 */
extern void expand_args(struct fuse_args *args,
			const char **long_names, size_t n_long_names,
			const char *end_expand);
			
/*
 * @brief checks the mount directory path
 *
 * Note: since it is using strerror (NOT thread safe) this MUST be used
 *       only in the initialisation phase before going multithreaded.
 *
 * @param path of the mountpoint to check
 * @return none
 */
extern void check_mount_path(const char *path);


/*
 * @brief logs the start date-time and start message
 *
 * Note: main should start with clock_gettime to pass the start time.
 *
 * @param pointer to time_t representing the start date
 * @return none
 */
extern void start_message(time_t *t);

/*
 * @brief extracts an integer from an argument string
 */
extern uint64_t read_int(const char *arg);

/*
 * @brief extracts and stores a string from a long argument, or -o fuse form.
 */
extern void read_str(const char *arg, char **where);

/*
 * @brief inserts a received work buffer into the private reader's queue.
 */
extern void prv_insert(struct node **head, struct node *rcv,
		       struct node *internal);

/*
 * @brief copy data from one buffer to the other when there is an overlap.
 */
extern size_t  move_buf(char *dst, off_t doff, size_t dsz,
			char *src, off_t soff, size_t ssz);

/*
 * @brief fills an incoming work buffer with already queued buffers.
 */
extern void inter_fill(struct reader *r, struct node *dst);

/*
 * @brief fills the queued buffers with a buffer read from the server.
 */
extern bool fill_buf(struct reader *r, char * buf, off_t offset, size_t size);

/*
 * @brief prune full buffers from the reader's private queue.
 */
extern void prune_queue(struct reader *r, struct node *internal);

/*
 * @brief curl callback for single range reading
 */
extern size_t single_range(char *ptr,
			   size_t size,
			   size_t nmemb,
			   struct node *n);
			   
/*
 * @brief init callback for fuse
 */
extern void *astreamfs_init(struct fuse_conn_info *conn);
