#
# astreamfs and derivative work (1fichierfs) Makefile
#
#
#  Copyright (C) 2018-2019  Alain BENEDETTI <alainb06@free.fr>
# 
#  License:
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 

ATOMIC := $(shell  ./test_atomic.sh) 

TARGET = astreamfs
LIBS := -lfuse $(ATOMIC) -pthread $(shell curl-config --libs)
CC = gcc
# CFLAGS := -g -Wall -D_FILE_OFFSET_BITS=64 -I/usr/include/fuse $(shell curl-config --cflags)
CFLAGS := -O2 -Wall -D_FILE_OFFSET_BITS=64 -I/usr/include/fuse $(shell curl-config --cflags)

.PHONY: default all clean

default: $(TARGET)
all: default

astreamfs_util.o: astreamfs_util.c astreamfs_util.h
	$(CC) $< $(CFLAGS) -c -I/usr/include/fuse -o $@
	
$(TARGET): astreamfs.c astreamfs_util.o astreamfs_util.h
	$(CC) $< $(CFLAGS) astreamfs_util.o $(LIBS) -o $@

1fichierfs_options.o: 1fichierfs_options.c astreamfs_util.h 1fichierfs.h
	$(CC) $< $(CFLAGS) -c -I/usr/include/fuse -o $@

1fichierfs_stats.o: 1fichierfs_stats.c astreamfs_util.h 1fichierfs.h
	$(CC) $< $(CFLAGS) -c -o $@

1fichierfs: 1fichierfs.c 1fichierfs.h astreamfs_util.h astream_engine.c \
	    astreamfs_util.o 1fichierfs_options.o 1fichierfs_stats.o
	$(CC) $< $(CFLAGS) 1fichierfs_options.o 1fichierfs_stats.o astreamfs_util.o $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)

install:
ifdef DESTDIR
	mkdir -p $(DESTDIR)/usr/bin/
	cp $(TARGET) $(DESTDIR)/usr/bin/
	mkdir -p $(DESTDIR)/usr/share/man/man1/
	cp $(TARGET).1.gz $(DESTDIR)/usr/share/man/man1/
	mkdir -p $(DESTDIR)/usr/share/man/fr/man1/
	cp $(TARGET).fr.1.gz $(DESTDIR)/usr/share/man/fr/man1/$(TARGET).1.gz
else
	cp $(TARGET) /usr/bin/
	cp man/$(TARGET).1.gz $(DESTDIR)/usr/share/man/man1/
	cp man/$(TARGET).fr.1.gz $(DESTDIR)/usr/share/man/fr/man1/$(TARGET).1.gz
endif

uninstall:
ifdef DESTDIR
	rm $(DESTDIR)/usr/bin/$(TARGET)
	rm $(DESTDIR)/usr/share/man/man1/$(TARGET).1.gz
	rm $(DESTDIR)/usr/share/man/fr/man1/$(TARGET).1.gz
else
	rm /usr/bin/$(TARGET)
	rm /usr/share/man/man1/$(TARGET).1.gz
	rm /usr/share/man/fr/man1/$(TARGET).1.gz
endif

