/*
 * astreamfs and derivative work (1fichierfs) common code
 *
 *
 * Copyright (C) 2018-2019  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 
 * Note, this code is not to be compiled separately, but to be included
 * in the source.
 * It relies on structures like filespec that are different from
 * astreamfs and 1fichiefs, thus can't easily be compiled separately (for now).
 *
 * Nevertheless, the code is common, hence the separation in a single file.
 *
 */


static size_t write_data(char *ptr,
			 size_t size,
			 size_t nmemb,
			 struct reader *r)
{
	size_t sz;
	struct node *n;
	bool locked = false;
	STREAM_STRUCT *cur_s;

	cur_s= r->s;
	sz = size * nmemb;

	/*
	 * It is too late to lock here! Therefore, it is possible that an
	 * incoming read has chosen to open a new stream instead of using this
	 * one. The result is still correct although less optimised. The price
	 * to avoid that is high: it would mean locking between iterations of
	 * this callback function and even before the curl_easy_perform!
	 */
	if (r->pos + sz > r->end) {
		r->end = r->pos + sz;
	}
	lprintf(LOG_DEBUG,
		"write_data[%d] <<in<< %p (%"PRIu64":%lu) | (%"PRIu64":%"PRIu64") n_rq=%d\n",
		r - readers, r->prv, r->pos, sz, r->start, r->end, r->n_rq );

	while(1) {
		unlock(&cur_s->fastmutex, &locked);
		if (NULL == r->prv || NULL != r->rcv)
			get_work(r);
	/* "timeout" case */
		if (NULL == r->prv) {
			lock(&cur_s->fastmutex, &locked);
			if (NULL != r->rcv) {
				continue;
			}
			r->idle = true;
			unlock(&cur_s->fastmutex, &locked);
			lprintf(LOG_INFO,
				"write_data[%d] time out: returning to idle state.\n",
				r - readers);
			r->streaming = false;
			return 0;
		}
		for (n = r->prv; n != NULL; n = n->next)
			lprintf(LOG_DEBUG,
			"write_data[%d] Dump: %p->%p  %"PRIu64":%lu-%lu\n",
			r - readers, n, n->next, n->offset, n->written, n->size);
	/* "close" case= useful when we are not at the end of file */
		if (0 == r->prv->size) {
			if (&work == r->prv->next)
				r->prv->next = work.next;
			if (NULL != r->prv->next)
				lprintf(LOG_CRIT,
					"write_data[%d] unexpected queued buffers after close: %p %"PRIu64":%lu-%lu\n",
					r - readers,
					r->prv->next,
					r->prv->next->offset,
					r->prv->next->written,
					r->prv->next->size);
			lock(&cur_s->fastmutex, &locked);
			sem_post(&r->prv->sem_done);
			r->idle = true;
			r->prv = NULL;
			unlock(&cur_s->fastmutex, &locked);
			r->streaming = false;
			return 0;
		}
	/* New stream on a another file */
		if (r->s != cur_s) {
			r->streaming = false;
			return 0;
		}
	/* Copy our buffer data inside the queued buffers */
		if (fill_buf(r, ptr, r->pos, sz)) {
			lock(&cur_s->fastmutex, &locked);
			prune_queue(r, &work);
			if (NULL != r->rcv) {
				continue;
			}
			if (NULL == r->prv) {
				r->start = r->pos;
				r->end = r->pos + sz;
				continue;
			}
			else {
				r->start = (r->prv->offset < r->pos) ?
					    r->prv->offset : r->pos;
				break;
			}
		}
	/* "normal streaming" case= consecutive buffers */
		if (r->prv->offset + r->prv->written == r->pos + sz) {
			if (r->start == r->prv->offset)
				break;
			lock(&cur_s->fastmutex, &locked);
			if (NULL == r->rcv)
				break;
			else
				continue;
		}
		unlock(&cur_s->fastmutex, &locked);
	/* "hole" case= we need to add the internal buffer */
		if (r->prv->offset >  r->pos + sz &&
		    r->prv->offset <= r->pos + sz + KBUF) {
			lprintf(LOG_DEBUG,"write_data[%d] hole1: %"PRIu64" %"PRIu64"-%lu\n",
				r - readers, r->prv->offset, r->pos, sz);
			work.size = r->prv->offset - r->pos;
			if (work.size > KBUF) {
				work.size = KBUF;
				work.offset = r->prv->offset - KBUF;
			}
			else {
				work.offset = r->pos;
			}
			work.next = r->prv;
			r->prv = &work;
			work.written = move_buf(work.buf,
						work.offset,
						work.size,
						ptr, r->pos, sz);
			lprintf(LOG_DEBUG,"write_data[%d] hole2: %"PRIu64":%lu-%lu\n",
				r - readers, work.offset, work.written, work.size);
			break;
		}
	/* last case here is "out of bound" = restart */
		unlock(&cur_s->fastmutex, &locked);

		/* rare case: hole + reverses buffers and we already got
		 * 	      passed the hole. In this case we have a
		 * 	      partially filled buffer and we continue
		 */
		if (0 != r->prv->written)
			break;
		r->streaming = false;
		return 0;
	}
	r->start = r->prv->offset;
	unlock(&cur_s->fastmutex, &locked);
	lprintf(LOG_DEBUG,
		"write_data[%d] >>out>> %p (%"PRIu64":%lu) > (%"PRIu64":%"PRIu64")\n",
		r - readers, r->prv, r->pos, sz, r->start, r->end );
	r->pos += sz;
	return sz;
}
