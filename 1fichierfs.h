/*
 * 1fichierfs: common header for 1fichierfs modules.
 *
 *
 * Copyright (C) 2018-2019  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define STREAM_STRUCT struct stream
#include "astreamfs_util.h"

#define PROG_NAME	"1fichierfs"
#define PROG_VERSION	"1.5.5" 


#ifdef __GNUC__
#define likely(x)	__builtin_expect(!!(x), 1)
#define unlikely(x)	__builtin_expect(!!(x), 0)
#else
#define likely(x)	(x)
#define unlikely(x)	(x)
#endif

struct pbcb {
	char  *pb;
	size_t cb;
};

struct parse_data {
	bool got_API_key;
	const char *mount_path;
	char *root;
	unsigned long sz_no_ssl;
};

struct params1f {
	long  curl_IP_resolve;
	char *api_key;
	unsigned long n_no_ssl;
	struct pbcb  *a_no_ssl;
	char *stat_file;
	char *refresh_file;
	unsigned long refresh_time;
	bool refresh_hidden;
	bool readonly;
	bool insecure;
	uid_t	uid;
	gid_t	gid;
};

enum api_routes {
	FOLDER_LS		= 0,
	DOWNLOAD_GET_TOKEN	= 1,
	USER_INFO		= 2,
	FILE_MV			= 3,
	FILE_CHATTR		= 4,
	FILE_RM			= 5,
	FILE_CP			= 6,
	FOLDER_MV		= 7,
	FOLDER_RM		= 8,
	FOLDER_MKDIR		= 9,
	FALLBACK_GET_TOKEN	=10,
};

enum refresh_cause {
	REFRESH_TRIGGER		= 0,
	REFRESH_HIDDEN		= 1,
	REFRESH_TIMER		= 2,
	REFRESH_MV		= 3,
	REFRESH_LINK		= 4,
	REFRESH_UNLINK		= 5,
	REFRESH_MKDIR		= 6,
	REFRESH_RMDIR		= 7,
	REFRESH_404		= 8,
	REFRESH_EXIT		= 9,
	REFRESH_INIT_ROOT	= 10,
};

struct strm_loc {
	unsigned long		i_loc;
	time_t			until;
	char			location[];
};

struct stream {
	_Atomic unsigned long	counter; /* nb_open +(1b:SP)+(1b:DEL)+(1b:ERR)*/
	unsigned long	 	access;  /* For shares when not ACCESS_RW     */
	long long	 	id;      /* in this case URL is the name      */
	char 			*email;
	unsigned long long	size;
	char			*URL;
	_Atomic (struct strm_loc *) loc;
	unsigned long		last_rq_id;
	bool			no_ssl;
	pthread_mutex_t		fastmutex;
	pthread_mutex_t		curlmutex;
	
	/* From there this is for statistics */
	unsigned long		nb_loc;
	_Atomic unsigned long	nb_streams;
	char			path[];
};


struct streams {
	unsigned long	n_streams;
	struct stream	*a_streams[];
};


#define STRLEN_MAX_INT64 20 /* Max length of int64 as string:
				  length=12345678901234567890
			       unsigned: 18446744073709551616
			       signed  : -9223372036854775807 */
#define KBUF (131072)		/* Readahead kernel buffer max size (128KB) */

	/* Live stream flags */
#define FL_ERROR	1
/* 1.0.9/1.2.0 This is to mark files as deleted on the tree and as deleted on
 *       the live list. It is thread safe (thanks to RCU) and does not require
 *       locks. It saves reading again the sub_dir for the tree management.   */
#define FL_DEL		2
#define FL_SPECIAL	(4)	     /* "Special" file, for the moment: stats */
#define OPEN_INC	8
#define OPEN_MASK	(~(FL_SPECIAL | FL_ERROR | FL_DEL))

/* Variable defined in 1fichierfs.c */
extern _Atomic (struct streams *)live;
extern struct params1f params1f;
extern struct fuse_operations unfichier_oper;
extern mode_t st_mode;

/* Variables and Functions defined in 1fichierfs_options.c */
extern const struct fuse_opt  unfichier_opts[];

int  unfichier_opt_count(void *, const char *, int, struct fuse_args *);
int  unfichier_opt_proc(void *,	const char *, int, struct fuse_args *);
void check_args_first_pass(struct parse_data *);
void check_args_second_pass(struct timespec *);
void check_args_third_pass(struct fuse_args *);


/* Functions defined in 1fichierfs_stats.c */
#define STAT_MAX_SIZE	KBUF

struct stats {
	unsigned long	counter; /* constant to FL_SPECIAL */
	size_t		size;
	char		buf[STAT_MAX_SIZE];
};

void init_stats(void);
struct stats *out_stats(struct stats *out);
void update_read_stats(int, unsigned int, struct timespec *);
void update_single_stats(int, struct timespec *);
void update_api_stats(enum api_routes, struct timespec *, long, unsigned int);
void update_refresh_stats(enum refresh_cause);
