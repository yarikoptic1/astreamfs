/*
 * 1fichierfs:	mount your 1fichier account (fuse).
 *		This work is derived from astreamfs that is a generic
 *		http(s) fuse reader. It uses the 1fichier's API to manage
 *		the directory tree and get download tickets.
 *
 * Copyright (C) 2018-2019  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Feature:
 *	Same as astreamfs but "specialised" to 1fichier
 *	On top of reading files from the server in a stream-optimised way,
 *	the APIs allow to:
 *	- Automatically read remote directory content and synchronize to
 *	  the server tree.
 *	- Report the usage of "Cold Storage" and compute available storage.
 *	- Delete files and directories (`rm` and `rmdir` commands)
 *	- Rename -and move- (`mv` command) files and directories
 *	- Hard link files only (`ln` command, no soft link, hard link only)
 *	- Create directories (`mkdir` command)
 *
 * Usage:
 *	Either
 *		read the help provided by: `1fichier -h` or
 *		look at the help section below, under astreamfs_opt_proc()
 *
 * Compile with:
 *	cc -Wall 1fichierfs.c	$(pkg-config fuse --cflags --libs)\
				$(curl-config --cflags --libs)\
				-o 1fichierfs -lpthread
 *
 * Compile dependancy:
 *	curl and fuse library
 *
 * Version: 1.5.5
 *
 *
 * History:
 *	2019/11/24: 1.5.5 Stats +contention FIX: engine (4 bugs) stats times (1)
 *	2019/11/22: 1.5.4 better and more resilient algo for download tokens.
 *	2019/11/10: 1.5.3 bug & crash fix on shares (+ optimised with --root)
 *	2019/10/30: 1.5.2 New: mount a path of the account + bug & crash fix.
 *	2019/10/17: 1.5.1 Crash: if ls.cgi API error / better options handling
 *	2019/08/06: 1.5.0 Shared folders including read-only + "hidden links".
 *	2019/06/10: 1.4.1 Crash: in size computation for stats read.
 *	2019/06/08: 1.4.0 New: added curl options --insecure and --cacert
 *	2019/06/08: 1.3.2 Crash:(Jaxx21@ubuntu-fr) typo with stats (parse_check)
 *	2019/06/03: 1.3.1 Fix: avg speed computation. Add: Memory stats.
 *	2019/05/28: 1.3.0 New: Statistics. Bug: engine broken on stream change.
 *	2019/05/22: 1.2.2 Fix bug: correctly display content of shared folders
 *	2019/05/19: 1.2.1 Fix crash: don't free streams when they are in use!
 *	2019/05/12: 1.2.0 Bugfixes (memory-management,...) / New no_ssl feature
 *	2019/05/11: 1.1.1 Fixed bugs on the first refactored version.
 *	2019/05/09: 1.1.0 Refactored live streams (avoid getting new DL-tickets)
 *	2019/04/28: 1.0.11 fix: arg subtype, JSON-esc names mv/cp,stat st_blocks
 *	2019/04/02: 1.0.10 ls: folders+files/New storage algo/Engine: header chk
 *	2019/03/24: 1.0.9 API chg: opti ls folder+files/ date ok dirs/ del optim
 *	2019/03/05: 1.0.8 API change: atomic and much simpler mv (folders)
 *	2019/03/04: 1.0.7 API change: atomic and much simpler ln (files)
 *	2019/02/28: 1.0.6 API change: atomic mv (files) in all cases
 *	2019/02/23: 1.0.5 API change: rename/move directory is now possible!
 *	2019/02/09: 1.0.4 info API changed; refr-time = min; protect refrsh-file
 *	2019/02/03: 1.0.3 Clean + make it compile and work on 32bits!
 *	2019/02/03: 1.0.2 API change: limit 1/5min user/info + get max storage.
 *	2019/01/21: 1.0.1 mkdir added.
 *	2019/01/19: 1.0.0 Last possible realistic implem: hard linking.
 *	2019/01/18: 0.9.7 Rename files in all cases (rename + move).
 *	2019/01/16: 0.9.6 Remove file (rm) and directory (rmdir) implemented.
 *	2019/01/13: 0.9.5 Fix bugs on statfs / (RW) Rename files same directory
 *	2019/01/05: 0.9.1 Resist to server error, retry (429) and free storage!
 *	2019/01/01: 0.9.0 Cope with server out of sync + Valgrinded bugs.
 *	2018/12/29: 0.8.7 Complete refresh: trigger, hidden and timer.
 *	2018/12/24: 0.8.6 First version with refresh and RCU freeing.
 *	2018/12/23: 0.8.5 Separate file and dir requests in preparation of RCU.
 *	2018/12/22: 0.8.4 Several bugfix + API Key can be specified in a file.
 *	2018/12/16: 0.8.3 Cleaning: moved common code to astreamfs_util.c/h
 *	2018/12/09: 0.8.2 "Good enough" initial specialized version, based on
 *			astreamfs as of version 0.8.2
 *
 *
 * Notes:
 *  Clocks: we use mainly CLOCK_MONOTONIC_COARSE, and CLOCK_REALTIME only when
 *   necessary, that is to display start time, and for sem_timedwait.
 *   To correctly time the 'download ticket' validity period, CLOCK_BOOTTIME
 *   should be used instead, because it takes care of suspend time. However,
 *   CLOCK_BOOTTIME is more than 50 times slower that CLOCK_MONOTONIC_COARSE,
 *   because it does a system call instead of using vdso (simulated system call)
 *   So it might happen, when resuming after suspend, that a 'download ticket'
 *   the program sees as valid is in fact invalid. Anyway, the server will
 *   respond with 410 (gone) and a new ticket will be requested. This
 *   possible short delays after resume are the price to pay for the
 *   simplification and optimisation. Anyway, there are many things that have
 *   to be "restarted" after resume, first one being the network!..
 *
 * May 2019 Refactoring:
 *  Information needed to display the directory tree and needed to stream are
 *  now separate. struct file gets the former, and struct stream the latter.
 *  Common parts: size and URL are copied on open().
 *  This allows:
 *    - To keep download tickets for their full duration (30 min) regardless
 *	of any refresh() that happened during the "ticket" validity period.
 *    - A more straightforward RCU free code since it does not need anymore to
 *	check if files are opened or not.
 *    - Lower memory footprint (on normal conditions assuming a reasonable
 *      amount of files are opened at the same time). This is also true because
 *      the 'st' structure (144 bytes in 64bits) is now replaced in 'file' by
 *      the only information we need for files: size and create date (16 bytes).
 *      That change alone saves 128 bytes for each file currently cached in the
 *      directory tree.
 *  Other changes:
 *    - The list of opened files is managed mainly at open().
 *    - refresh() takes care of potential files that were marked as ERROR.
 *	This is a feature.
 *	When there is a read error on a file, it is marked as error and EIO or
 *	EREMOTEIO are returned. Subsequent open() of the same file return
 *	EACCESS. Any refresh will purge the files that were marked as error
 *	from the list, allowing the user to retry.
 *    - release() only decrements the opened counter.
 *    - Also release() does NOT send the close signal to the async_reader unless
 *	the file is marked as error. This means that this stream will stay live
 *	for 45 seconds and could serve requests for the same file. That works
 *	well for files under 16K (curl default buffer), they will be served
 *	from memory within the 45 seconds regardless of kernel cache parameter.
 *    - This change also makes open() and release() completely lockless.
 *	Note that open() is not waitless since the new 'live' (opened and
 *	alive) 'streams' struct is managed with atomics & RCU (no mutex locks).
 *
 * Note: using only the CA that signs *.1fichier.com certificates greatly
 *       reduces the memory footprint on GnuTLS (around 32MB instead of 96MB!)
 *       To make this CA file, look at the curl documentation or do:
$ openssl s_client -showcerts -servername 1fichier.com -connect 1fichier.com:443 >1fichier.pem
 *       Then extract from that file the first certificate (--begin/--end cert)
 */

// TODO: we really need a test program/shell to avoid regressions!

#define _GNU_SOURCE 
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stddef.h>
#include <sys/statvfs.h>

#include "1fichierfs.h"

/*******************************************************
 *  These are the things you can change at compile time.
 */

/*  See 1fichier.h to change that:
#define PROG_NAME	"1fichierfs"
#define PROG_VERSION	"1.2.0"
*/
       const char log_prefix[] = PROG_NAME;	/* Prefix for log messages    */

extern const char def_user_agent[];		/* See 1fichierfs_options.c   */
extern const char def_type[];			/* See 1fichierfs_options.c   */
extern const char def_fsname[];			/* See 1fichierfs_options.c   */

						/* Default initial size (on 
						 * stack) of the JSON buffer  */
#define DEF_JSON_BUF_SIZE (16 * 1024 + 1)       /* This is not a random number!
						 * By default, curl callback
						 * read buffers are 16k max.
						 * So when a json response fits
						 * in a single curl callback
						 * no malloc is needed. +1 for
						 * the '\0' end string.
						 * That should be the case of
						 * almost all requests except
						 * long directory listings (50+
						 * files + sub-directories).  */

						/* Upon HTTP 429 (too many
						 * requests), this sets delays
						 * and number of retries.     */
static const useconds_t retry_delay_ms[]= { 350000, 700000, 1000000 };

						
#define DELAY_KEEP_UNUSED_STREAM (300)		/* How long (sec) to keep
						 * unsused files: with no read
						 * = never had 'location'     */


			/* NOTE: MAX_READERS (defaults to 4) can also be
			 * 	 changed. it is located in astreamfs_util.h   */

static const char hidden_url[] = "{hidden}";

/********************************************************
 *  Do NOT change the following definitions or variables.
 *  Things would break!
 *  These definitions and variables are either system dependent,
 *  or depend on how 1fichier works.
 */

#define ST_BLK_SZ (512)		/* This is the block size stat's st_blocks */
#define DEFAULT_BLK_SIZE (4096)	/* This is the block size for statvfs */

#define URL_API_EP "https://api.1fichier.com/v1/"
static const char url_api_ep[]		= URL_API_EP;
static const char *api_urls[] = {
/*	FOLDER_LS		= 0, */ URL_API_EP"folder/ls.cgi",
/*	DOWNLOAD_GET_TOKEN	= 1, */ URL_API_EP"download/get_token.cgi",
/*	USER_INFO		= 2, */ URL_API_EP"user/info.cgi",
/*	FILE_MV			= 3, */ URL_API_EP"file/mv.cgi",
/*	FILE_CHATTR		= 4, */ URL_API_EP"file/chattr.cgi",
/*	FILE_RM			= 5, */ URL_API_EP"file/rm.cgi",
/*	FILE_CP			= 6, */ URL_API_EP"file/cp.cgi",
/*	FOLDER_MV		= 7, */ URL_API_EP"folder/mv.cgi",
/*	FOLDER_RM		= 8, */ URL_API_EP"folder/rm.cgi",
/*	FOLDER_MKDIR		= 9, */ URL_API_EP"folder/mkdir.cgi",
};

static const char *refresh_msgs[] = {
/*	REFRESH_TRIGGER		= 0, */ "trigger",
/*	REFRESH_HIDDEN		= 1, */ "hidden",
/*	REFRESH_TIMER		= 2, */ "timer",
/*	REFRESH_MV		= 3, */ "move",
/*	REFRESH_LINK		= 4, */ "link",
/*	REFRESH_UNLINK		= 5, */ "unlink",
/*	REFRESH_MKDIR		= 6, */ "mkdir",
/*	REFRESH_RMDIR		= 7, */ "rmdir",
/*	REFRESH_404		= 8, */ "HTTP 404",
/*	REFRESH_EXIT		= 9, */ "exit",
/*	REFRESH_INIT_ROOT	=10, */ "init_root",
};


/* These are constants set to 1fichier timeouts */
#define CURL_KEEPALIVE_TIME	( 20L)
#define CURL_IDLE_TIME		(  45)	/* As of 1.3.0: Strange behaviour when
					   reusing a "keep-alived" TCP stream 
					   after 1 min, there is ~20 sec delay
					   to restart (or even sometimes curl
					   error 50: RCV_ERROR). Keeping readers
					   only for 45 seconds avoids the issue,
					   at the expense of more 'curling'
					   which is faster than waiting
					   ~20/25 seconds!
					    Was: 300 for 1fichier, 295 safer! */
#define CURL_KEEP_LOCATION_TIME (1800)  /* 30 minutes as documented.          */
#define CURL_CNX_TIMEOUT	( 30L)

#define STREAM_ERROR ((CURLcode)999)	/* CURL errors stop around 90 */

/*****************************************/

struct params1f params1f= {
/* curl_IP_resolve */	CURL_IPRESOLVE_WHATEVER,
/* api_key	   */	NULL,
/* n_no_ssl	   */	0UL,
/* a_no_ssl	   */	NULL,
/* stat_file	   */	NULL,
/* refresh_file	   */	NULL,
/* refresh_time	   */	0UL,
/* refresh_hidden  */	false,
/* readonly	   */	false,
/* uid		   */	-1,
/* gid		   */	-1,
	  };

mode_t st_mode;

_Atomic bool exiting = false;
_Atomic unsigned long refresh_start;


#define S_ERROR(s) (0 != \
	(atomic_load_explicit(&((s)->counter), \
			      memory_order_acquire) & FL_ERROR))
#define F_DEL(f) (0 != \
	(atomic_load_explicit(&((f)->flags), \
			      memory_order_acquire) & FL_DEL))


_Atomic (struct streams *)live = NULL;

struct file {
	const char		*URL;
	const char		*filename;
	time_t			cdate;
	unsigned long long	size;
	_Atomic unsigned int	flags;
};

#define ACCESS_RW	(0)
#define ACCESS_RO	(1)
#define ACCESS_HIDDEN	(2)
#define ACCESS_MASK	(3)
#define ACCESS_SHIFT	(2)

#define IS_HIDDEN(a) (ACCESS_HIDDEN == ((a) & ACCESS_MASK))
#define IS_SHARED_ROOT(d) (0 == d->id && ACCESS_RW != d->access)
#define SHARER_EMAIL(d) (d->name + (d->access >> ACCESS_SHIFT))

/* The access field is ACCESS_RW (0) when not on a shared directory.
 * On the root of the share, the field holds the offset of the sharer's
 * e-mail on the name field, shifted 2 positions, and the last 2 bits are
 * one of above: ACCESS_RW, ACCESS_RO or ACCESS_HIDDEN.
 * For descendants of the shares, they inherit only these last 2 bits.
 */

struct dir {
	_Atomic (struct dentries *) dent;
	pthread_mutex_t	lookupmutex;
	char		*name;
	long long	id;
	time_t		cdate;
	unsigned long	access; /* offset of email | last 2 bits access mode */
};

struct dentries {
	unsigned long	n_subdirs;
	unsigned long	n_files;
	struct dir	*subdirs;
	struct file	files[];
};

static char def_root[] =  "/";

static struct dir root = {
/*	*d		*/	NULL,
/*	lookupmutex	*/	PTHREAD_MUTEX_INITIALIZER,
/*	name		*/	def_root,
/*	id		*/	0,
/*	cdate		*/	0,
/*	access		*/	ACCESS_RW,
			  };

/* Note: 1fichier has put a hard limit of minimum time 5 minutes between calls
 *	 to user_info. Calling more often triggers a 403, and if too many
 *	 calls are made despite the 403, the user is temporarily banned!
 *	 We could RCU the storage values, but for the moment it is not worth
 *	 it, a simple mutex (needed anyway) seems enough.
 *	 Hence all storage related variable don't need to be atomic, they
 *	 are read/written under lock.
 */
#define USER_INFO_DELAY (305) /* 5min + 5sec to be sure: time of req + round */
#define FL_STOR_ERROR	1
#define FL_STOR_REFRESH 2

static pthread_mutex_t	storage_mutex = PTHREAD_MUTEX_INITIALIZER;
static off_t		avail_storage;
static off_t		max_storage;
static time_t		storage_next_update = 0;
static unsigned int	storage_flags = FL_STOR_REFRESH;

/* Variables and define used when fallback on download/get_token API is needed.
 */
static unsigned long	fallback_tested = 0;
static time_t		fallback_until  = 0;
#define KEEP_FALLBACK_TIME (300)


static __thread struct node work = {NULL,NULL};

struct reader readers[MAX_READERS];


struct walk {
	bool		is_dir;
	union {
		struct dir	*dir;
		struct file	*fs;
	} res;
	struct dir	*parent;
};

struct json {
	char  *pb;
	size_t cb;
	char   buf[DEF_JSON_BUF_SIZE];
};


/*
 *  Instead of pulling the liburcu dependancy, for the moment we do
 *  a simplified processing.
 *  The cleaning of structures is done when we observe zero thread in
 *  the read critical section. This is a stronger condition than what
 *  liburcu does, but as long as there is a "reasonable" usage of the
 *  mount, it will only defer from a "reasonable" time the freeing of buffers.
 *  We still use the liburcu function names, so that replacing it with the
 *  read stuff at some point should be easy.
 *  What also makes less efficient to use the real liburcu is that we have
 *  no control on fuse threads that have critical sections. The safest path is
 *  then to issue rcu_register_thread and rcu_read_lock at the same time, and
 *  same for unlock.
 */
struct rcu_head {
	struct rcu_head *next;
	void (*func)(struct rcu_head *head);
};
static _Atomic (struct rcu_head *) rcu_defered = NULL;
static _Atomic unsigned long rcu_count = 0 ;

#define rcu_init()
#define rcu_exit()
#define rcu_read_lock()
#define rcu_read_unlock()

struct rcu_free_d {
	struct rcu_head  head;
	struct dentries	*dent;
};
struct rcu_free_p {
	struct rcu_head head;
	void		*p;
};

static void rcu_free_queue(_Atomic (struct rcu_head *)*head)
{
	struct rcu_head *prv, *next;
	msg_pop((_Atomic (struct node *)*)head,
		(struct node **)&prv);
	if (NULL == prv)
		return;
	lprintf(LOG_DEBUG, "RCU Defered cleaning.\n");
	do {
		next = prv->next;
		prv->func(prv);
		fs_free(prv);
		prv = next;
	} while(NULL != prv);
}

static void rcu_register_thread(void) {
	atomic_fetch_add_explicit(&rcu_count, 1UL, memory_order_acq_rel);
}
static void rcu_unregister_thread(void) {
	unsigned long n;
	n = atomic_fetch_sub_explicit(&rcu_count, 1UL, memory_order_acq_rel);
	if (1 == n) {
		rcu_free_queue(&rcu_defered);
	}
}
static void call_rcu(struct rcu_head *head,
		     void (*func)(struct rcu_head *head)) {
	head->func = func;
	msg_push((_Atomic (struct node *)*)&rcu_defered, (struct node *)head);
}

static void rcu_free_dentries(struct dentries *dent)
{
	unsigned long i;

	for (i = 0; i < dent->n_subdirs; i++) {
		if (NULL != dent->subdirs[i].dent) {
			lprintf(LOG_DEBUG,
				"Freeing dentries for subdir: %s/%p\n",
				dent->subdirs[i].name,
				dent->subdirs[i].dent);
			rcu_free_dentries(dent->subdirs[i].dent);
		}
	}

	lprintf(LOG_DEBUG, "Freeing dentries: %p\n", dent);
	fs_free(dent);
}

static void rcu_clean(struct rcu_head *head)
{
	rcu_free_dentries(((struct rcu_free_d *)head)->dent);
}

static void rcu_free_ptr(struct rcu_head *head)
{
	struct rcu_free_p *h = (struct rcu_free_p *)head;
	lprintf(LOG_INFO, ">> rcu_free_ptr: pointer=%p\n", h->p);
	fs_free(h->p);
}

static size_t get_json_response( const char *ptr, const size_t size,
				 const size_t nmemb, void *userdata)
{
	size_t chunk = size * nmemb;
	struct json *j = (struct json *)userdata;

	if ( j->cb + chunk + 1 > DEF_JSON_BUF_SIZE ) {
		if (j->cb < DEF_JSON_BUF_SIZE) {
			j->pb = fs_alloc(j->cb + chunk + 1);
			memcpy(j->pb, j->buf,j->cb);
		}
		else {
			j->pb = fs_realloc(j->pb, j->cb + chunk + 1);
			if ( NULL == j->pb )
			lprintf(LOG_CRIT,
				"out of memory. Cannot allocate %lu bytes.\n",
				j->cb + chunk + 1);
		}
	}
	memcpy(j->pb + j->cb, ptr, chunk);
	j->cb += chunk;

	return size * nmemb;
}

static void json_init(struct json *j)
{
	j->cb = 0;
	j->pb = j->buf;
}

static void json_free(struct json *j)
{
	if (j->pb != j->buf)
		fs_free(j->pb);
}

/* TODO use a real JSON parser instead of strstr
 *   Instead, to search the member "foo", we search directly "foo", since we
 *   know keys are NOT escaped by 1fichier. We also check that after "foo"
 *   we find some whitespace then a ':'. Otherwise we loop since "foo" could
 *   also have been a value.
 *   That works because :
 *	- keys are NOT escaped by 1fichier
 *	- valid json cannot contain "foo" inside a string (it would have to
 *	  be escaped like "bar\"foo\"bar", hence strstr won't find "foo")
 *
 */

#define JSON_STR_ERR ((size_t)-1)
static const char json_item_start[]= "{\"";
static const char json_escapes[] = "\"\\/bfnrt";
static const char json_sub_folders[]= "\"sub_folders\"";
static const char json_items[]= "\"items\"";
static const char json_shares[]= "\"shares\"";
static const char json_filename[]= "\"filename\"";
static const char json_url[]= "\"url\"";
static const char json_email[]= "\"email\"";
static const char json_hide_links[]= "\"hide_links\"";
static const char json_rw[]= "\"rw\"";
static const char json_size[]= "\"size\"";
static const char json_date[]= "\"date\"";
static const char json_name[]= "\"name\"";
static const char json_id[]= "\"id\"";
static const char json_cdate[]= "\"create_date\"";
static const char json_cold_storage[]= "\"cold_storage\"";
static const char json_hot_storage[]= "\"hot_storage\"";
static const char json_allowed_cold_storage[]= "\"allowed_cold_storage\"";
static const char json_status[]= "\"status\"";
static const char json_OK[]= "\"OK\"";

static char *json_find_key(const char *json, const char *str)
{
	char *p;
	do
	{
		p = strstr(json, str);
		if (NULL == p)
			return NULL;
		p += strlen(str);
		while ( *p == '\x09' || *p == '\x0A' ||
			*p == '\x0D' || *p == ' ')
			p++;
	} while (':' != *p);
	p++;
	while ( *p == '\x09' || *p == '\x0A' ||
		*p == '\x0D' || *p == ' ')
		p++;
	return p;
}


static size_t json_strlen(const char *json)
{
	const char *p = json;
	size_t	s = 0;
	if (NULL == p || '"' != *p)
		return JSON_STR_ERR;
	for (p++; '\0' != *p && '"' != *p; p++, s++)
	{
		if ('\\' == *p) {
			p++;
			if ('u' == *p)
				return JSON_STR_ERR;
				/* TODO skip \uXXXX + count chars out! */
			else
				if (NULL == strchr(json_escapes, *p))
					return JSON_STR_ERR;

		}
	}
	return s;
}

static char *json_strcpy(const char *json, char *dest)
{
	const char *p = json;
	if (NULL == p || '"' != *p)
		return NULL;
	for (p++; '\0' != *p && '"' != *p; p++, dest++)
	{
		if ('\\' == *p) {
			p++;
			switch (*p) {
				case '"' : *dest = '"';
					   break;
				case '\\': *dest = '\\';
					   break;
				case '/' : *dest = '/';
					   break;
				case 'b' : *dest = '\x08';
					   break;
				case 'f' : *dest = '\x0C';
					   break;
				case 'n' : *dest = '\x0A';
					   break;
				case 'r' : *dest = '\x0D';
					   break;
				case 't' : *dest = '\x09';
					   break;
				case 'u' :
				default	 :
					   return NULL;
			}
		}
		else {
			*dest = *p;
		}
	}
	*dest = '\0';
	return dest + 1;
}

static char *json_escape(const char *str)
{
	unsigned int i = 0, j = 0;
	const char *p;
	char *q;

	for (p = str; '\0' != *p; p++, j++)
		if ('\\' == *p || '"' == *p)
			i++;
	if (0 == i)
		return (char *)str;

	q = fs_alloc(j + i + 1);
	for (j = 0, p = str; '\0' != *p; p++, j++) {
		if ('\\' == *p || '"' == *p) {
			q[j] = '\\';
			j++;
		}
		q[j] = *p;
	}
	q[j] = '\0';
	return q;
}

static int cmp_files(const void *f1,const void *f2)
{
	return strcmp(((const struct file *)f1)->filename,
		      ((const struct file *)f2)->filename);
}

static void init_fs(struct file *fs)
{
	atomic_store_explicit(&fs->flags, 0, memory_order_relaxed);
}

static int cmp_dirs(const void *d1,const void *d2)
{
	return strcmp(((const struct dir *)d1)->name,
		      ((const struct dir *)d2)->name);
}

static char *parse_check(char  *ptr, struct dir *d, const char * msg) {
	if (NULL == ptr)
		return NULL;
	if ( '[' == *ptr ) {
		*ptr = '\0';
		return ptr + 1;
	}
	else {
		lprintf(LOG_ERR,
			"Ignoring: %s of `%s` id=%"PRId64", is not an array!\n",
			msg, d->name, d->id);
		return NULL;
	}
}

static void parse_err(const char * pattern, struct dir *d)
{
	lprintf(LOG_ERR,
		"Ignoring: error parsing %s of `%s` id=%"PRId64"\n",
		pattern, d->name, d->id);
}

static size_t json_value_lenght(const char *json, const char *key, char **val)
{
	char *p;
	size_t sz;

	if (NULL != val)
		*val = NULL;
	p = json_find_key(json, key);
	if (NULL == p)
		return 0;
	sz = json_strlen(p);
	if (NULL != val)
		*val = p + 1;
	return (JSON_STR_ERR == sz) ? 0 : sz;
}

static char name_email_link[]= " on ";

static unsigned long parse_count(const char *s, const char *pattern1,
				 const char *pattern2, size_t add,
				 size_t *s_str, bool *f_refresh, bool *f_stat,
				 struct dir *d)
{
	unsigned long n;
	char *p, *q, *val;
	size_t t, sz = 0, r1 = 0, r2 = 0;
	bool f_r = *f_refresh, f_s = *f_stat;

	if (NULL == s)
		return 0;
	p = strstr(s, json_item_start);
	if (NULL == p)
		return 0;
	if (f_r)
		r1 = strlen(params1f.refresh_file);
	if (f_s)
		r2 = strlen(params1f.stat_file);
	n = 0;
	while(true) {
		q = strstr(p + sizeof(json_item_start), json_item_start);
		if (NULL != q)
			*q = '\0';
		t = json_value_lenght(p, pattern1, &val);
		if (0 == t) {
			parse_err(pattern1, d);
			return 0;
		}
		sz += t + 1;
		if (f_r && r1 == t
			&& 0 == strncmp(val, params1f.refresh_file, r1)) {
			lprintf(LOG_WARNING,
				"refresh triggger off: a file or directory `/%s` already exists in `%s`.\n",
				params1f.refresh_file, d->name);
			*f_refresh = f_r = false;
		}
		if (f_s && r2 == t
			&& 0 == strncmp(val, params1f.stat_file, r2)) {
			lprintf(LOG_WARNING,
				"statistics off: a file or directory `/%s` already exists in `%s`.\n",
				params1f.stat_file, d->name);
			*f_stat = f_s = false;
		}
		
		if (NULL != pattern2) {
			t = json_value_lenght(p, pattern2, NULL);
			if (0 == add) {
				if (0 == t) {
					parse_err(pattern2, d);
					return 0;
				}
				sz += t + 1;
			}
			else {
				if (0 != t)
					sz += t + add;
			}
		}
		n++;
		if (NULL == q)
			break;
		*q = json_item_start[0];
		p = q;
	}
	*s_str = sz;
	return n;
}

static bool fill_folders(char *start, struct dentries *dent, char *s,
			 unsigned int access)
{
	char *p, *q;
	struct dir *x;
	struct tm tm;

	x = dent->subdirs;
	p = strstr(start, json_item_start);
	if (NULL == p) {
		lprintf(LOG_ERR, "unexpected condition in fill_folders.\n");
		return false;
	}
	while(true) {
		start = strstr(p + sizeof(json_item_start), json_item_start);
		if (NULL != start)
			*start = '\0';
		x->dent = NULL;
		pthread_mutex_init(&x->lookupmutex, NULL);
		x->id = 0;
		q = json_find_key(p, json_id);
		if (NULL == q) {
			lprintf(LOG_ERR, "could not find id for dir: %s\n",
				x->name);
			return false;
		}
		else {
			errno = 0;
			x->id = strtoll(q, NULL, 10);
			if (0 != errno) {
				lprintf(LOG_ERR, "error reading id for dir: %s\n",
					x->name);
				return false;
			}
		}
		x->name = s;
		s = json_strcpy(json_find_key(p, json_name), s);
		q = json_find_key(p, json_email);
		if (NULL == q) {
			x->access = access;
		}
		else {
			memcpy(s - 1,
			       name_email_link,
			       sizeof(name_email_link) - 1);
			x->access = s - x->name + sizeof(name_email_link) - 2;
			s = json_strcpy(q, x->name + x->access);
			x->access <<= ACCESS_SHIFT;
			q = json_find_key(p, json_hide_links);
			if (NULL != q && '1' == *q) {
				x->access += ACCESS_HIDDEN;
			}
			else {
				q = json_find_key(p, json_rw);
				if (NULL != q && '1' == *q)
					x->access += ACCESS_RW;
				else
					x->access += ACCESS_RO;
			}
		}
		x->cdate = params.mount_st.st_ctim.tv_sec;
		
		/* Don't read create_date on root shares: they don't have one!*/
		if (0 != x->id || ACCESS_RW == x->access) {
			q = json_find_key(p, json_cdate);
			if (NULL == q || '"' != *q) {
				lprintf(LOG_WARNING,
					"could not find create_date for dir: %s\n",
					x->name);
			}
			else {
				q= strptime(q + 1, "%Y-%m-%d %H:%M:%S", &tm);
				if ( NULL == q || '"' != *q) {
					lprintf(LOG_WARNING,
						"error reading create_date for dir: %s\n",
						x->name);
				}
				else {
					tm.tm_isdst = 0;
					x->cdate = mktime(&tm);
				}
			}
		}
		
		x++;
		if (NULL == start)
			break;
		*start = json_item_start[0];
		p = start;
	}

	qsort(dent->subdirs, dent->n_subdirs, sizeof(struct dir), cmp_dirs);

	return true;
}

static bool fill_files(char *start, struct dentries *dent, char *s, bool hidden)
{
	char *p, *q;
	struct file *x;
	struct tm tm;

	if (NULL == start)
		return true;

	for (x = dent->files;
	     NULL != (p = strstr(start, json_item_start));
	     x++, start = p + sizeof(json_item_start)) {
		init_fs(x);
		if (hidden) {
			x->URL = hidden_url;
		}
		else {
			x->URL = s;
			s = json_strcpy(json_find_key(p, json_url), s);
		}
		x->filename = s;
		s = json_strcpy(json_find_key(p, json_filename), s);
		q = json_find_key(p, json_size);
		if (NULL == q) {
			lprintf(LOG_ERR, "could not find size for file: %s\n",
				x->filename);
			return false;
		}
		else {
			errno = 0;
			x->size = strtoll(q, NULL, 10);
			if (0 != errno) {
				lprintf(LOG_ERR, "error reading size for file: %s\n",
					x->filename);
				return false;
			}
		}
		q = json_find_key(p, json_date);
		if (NULL == q || '"' != *q) {
			lprintf(LOG_ERR, "could not find date for file: %s\n",
				x->filename);
			return false;
		}
		else {
			q= strptime(q + 1, "%Y-%m-%d %H:%M:%S", &tm);
			if ( NULL == q || '"' != *q) {
				lprintf(LOG_ERR, "error reading date for file: %s\n",
					x->filename);
				return false;
			}
			else {
				tm.tm_isdst = 0;
				x->cdate = mktime(&tm);

			}
		}
	}

	return true;
}

static void parse_all(struct dir *d, struct json *j, struct dentries **dent)
{
	char    *subdirs = NULL,  *files = NULL;
	size_t s_subdirs = 0   , s_files = 0;
	char *s;
	const char *url_pattern;
	unsigned long n_subdirs, n_files, i;
	bool f_refresh, f_stat, f_hidden;

	f_refresh = (&root == d	 && NULL != params1f.refresh_file);
	f_stat    = (&root == d	 && NULL != params1f.stat_file);

	if (NULL != j) {
	/* Removing text inside "shares" to avoid fooling the algorithm.
	 * Assuming there is no ']' in shares. The only place it could
	 * be is in the e-mail string. ']' is allowed in an e-mail with
	 * restrictions so at the moment this will fail and is a //TODO
	 * to be merged with "use a real JSON parser!". */
		s = j->pb;
		while (NULL != (s = json_find_key(s, json_shares))) {
			if ( '[' != *s++ )
				continue;
			while (']' != *s && '\0' != *s)
				*s++ = ' ';
		}

		subdirs = json_find_key(j->pb, json_sub_folders);
		files	= json_find_key(j->pb, json_items);
		subdirs = parse_check(subdirs, d, json_sub_folders);
		files	= parse_check(files  , d, json_items);
	}

	n_subdirs = parse_count(subdirs, json_name, json_email,
				sizeof(name_email_link) - 1,
				&s_subdirs, &f_refresh, &f_stat, d);
	f_hidden = IS_HIDDEN(d->access);
	if (f_hidden)
		url_pattern = NULL;
	else
		url_pattern = json_url;
	n_files	  = parse_count(files  , json_filename, url_pattern, 0,
				&s_files  , &f_refresh, &f_stat, d);

	if (f_refresh) {
		if (params1f.refresh_hidden)
			f_refresh = false;
		else
			n_files++;
	}
	if (f_stat)
		n_files++;

	*dent = fs_alloc(sizeof(struct dentries) + s_subdirs + s_files +
			 sizeof(struct dir) * n_subdirs +
			 sizeof(struct file) * n_files);

	(*dent)->n_subdirs = n_subdirs;
	(*dent)->n_files   = n_files;
	(*dent)->subdirs   = (struct dir *)&(*dent)->files[n_files];

	s = (char *)(&(*dent)->subdirs[n_subdirs]);
	if (0 != (*dent)->n_subdirs && 
	    ! fill_folders(subdirs, *dent, s, d->access & ACCESS_MASK) )
		(*dent)->n_subdirs = 0;

	if (0 != (*dent)->n_files) {
		if ( fill_files(files, *dent, s + s_subdirs, f_hidden) ) {
			i = n_files - ((f_refresh) ? 1:0) - ((f_stat) ? 1:0);
			if (f_refresh) {
				init_fs(&(*dent)->files[i]);
				(*dent)->files[i].size = 0;
				(*dent)->files[i].cdate =
						params.mount_st.st_ctim.tv_sec;
				(*dent)->files[i].URL =
				(*dent)->files[i].filename =
						(char *)params1f.refresh_file;
				i++;
			}
			if (f_stat) {
				init_fs(&(*dent)->files[i]);
				(*dent)->files[i].size = STAT_MAX_SIZE;
				(*dent)->files[i].cdate =
						params.mount_st.st_ctim.tv_sec;
				(*dent)->files[i].URL = NULL;
				(*dent)->files[i].filename =
						(char *)params1f.stat_file;
			}
			qsort((*dent)->files, (*dent)->n_files,
			      sizeof(struct file), cmp_files);

		}
		else {
			(*dent)->n_files = 0;
		}
	}

	atomic_store_explicit(&d->dent, *dent, memory_order_relaxed);
}

/*
 * @brief utility to initialise a curl handle
 *
 * The handle is initialised and populated according to the global parameters
 *
 * @param none
 * @return the initialised curl handle
 */
static CURL *curl_init()
{
	CURL *curl;

	curl = curl_easy_init();
	if (NULL == curl)
		lprintf( LOG_CRIT, "initializing curl easy handle.\n" );
	CURL_EASY_SETOPT(curl, CURLOPT_IPRESOLVE, params1f.curl_IP_resolve
			     , "%ld");
	if (NULL != params.user_agent)
		CURL_EASY_SETOPT(curl, CURLOPT_USERAGENT, params.user_agent,
				 "%s");
	if (params1f.insecure)
		CURL_EASY_SETOPT(curl, CURLOPT_SSL_VERIFYPEER, 0, "%ld");
	if (NULL != params.ca_file)
		CURL_EASY_SETOPT(curl, CURLOPT_CAINFO, params.ca_file, "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPALIVE, 1L,"%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPIDLE, CURL_KEEPALIVE_TIME,"%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPINTVL,CURL_KEEPALIVE_TIME,"%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_NOSIGNAL, 1L,"%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_CONNECTTIMEOUT, CURL_CNX_TIMEOUT,"%ld");

	return curl;
}


static long curl_json_perform_wrapped(	CURL *curl, struct curl_slist **list,
					char *postdata, struct json *j,
					enum api_routes rt,
					const char *name_msg)
{
	CURLcode res;
	long http_code;

	if (NULL == *list) {
		*list = curl_slist_append(NULL, params1f.api_key);
		if (NULL != *list)
			*list = curl_slist_append(*list,
					"Content-Type: application/json");
		if (NULL == *list)
			lprintf(LOG_CRIT,
				"building headers (curl_slist_append).\n");

		CURL_EASY_SETOPT(curl, CURLOPT_HTTPHEADER, *list, "%p");
		CURL_EASY_SETOPT(curl, CURLOPT_WRITEFUNCTION, get_json_response,
						 "%p");
		CURL_EASY_SETOPT(curl, CURLOPT_WRITEDATA, (void *)j, "%p");
	}
	CURL_EASY_SETOPT(curl, CURLOPT_POSTFIELDS, postdata, "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_URL, api_urls[rt], "%s");

	res = curl_easy_perform(curl);

	j->pb[j->cb]='\0';
	if (CURLE_OK != res) {
		lprintf(LOG_ERR,
			"Ignoring: error %ld on curl_easy_perform url=`%s` name=`%s`.\n",
			res, api_urls[rt], name_msg);
			return 0;
	}
	else {
		CURL_EASY_GETINFO(curl, CURLINFO_RESPONSE_CODE, &http_code);
		if (HTTP_OK != http_code) {
			lprintf(LOG_ERR,
				"Ignoring: (http_code: %ld) url=`%s` name=`%s`.\n",
				http_code, api_urls[rt], name_msg);
		}
		else {
			char *p;
			p = json_find_key(j->pb, json_status);
			if (NULL == p ||
			    0 != strncmp(p, json_OK, sizeof(json_OK) - 1)) {
				lprintf(LOG_ERR,
					"Ignoring: status is NOT OK, url=`%s` name=`%s` response=%s.\n",
					api_urls[rt], name_msg, j->pb);
				return 0;
			}
		}
	}

	return http_code;
}

static _Atomic unsigned long n_API_calls = 0;
static _Atomic bool throttled = false;
static pthread_mutex_t	apimutex = PTHREAD_MUTEX_INITIALIZER;

#define MAX_RETRY (sizeof(retry_delay_ms)/sizeof(retry_delay_ms[0]))

static long curl_json_perform(	CURL *curl, struct curl_slist **list,
				char *postdata, struct json *j,
				enum api_routes rt, const char *name_msg)
{
	unsigned long i_API_call;
	long http_code;
	unsigned int retry;
	struct timespec start;
	bool too_many_rq = false;
	bool locked = false;
	
	if (NULL != params1f.stat_file)
		clock_gettime(CLOCK_MONOTONIC, &start);

	i_API_call = atomic_fetch_add_explicit(&n_API_calls, 1L,
					       memory_order_acq_rel) + 1L;
	lprintf(LOG_INFO, "<<< API(in) (iReq:%lu) %s POST=%s name=%s\n"
			, i_API_call
			, api_urls[rt] + sizeof(url_api_ep) - 1
			, postdata
			, name_msg);

	too_many_rq = atomic_load_explicit(&throttled, memory_order_acquire);
	if (too_many_rq)
		lock(&apimutex, &locked);

	for (retry = 0; retry < MAX_RETRY; retry++) {
		http_code = curl_json_perform_wrapped(curl, list, postdata,
						      j, rt, name_msg);
		if (HTTP_TOO_MANY_REQUESTS == http_code) {
			if (!too_many_rq) {
				too_many_rq = true;
				atomic_store_explicit(&throttled, true,
						      memory_order_release);
				lock(&apimutex, &locked);
			}
			usleep(retry_delay_ms[retry]);
		}
		else {
			if (too_many_rq && 0 == retry)
				atomic_store_explicit(&throttled, false,
						      memory_order_release);
			break;
		}
	}
	unlock(&apimutex, &locked);
	if (NULL != params1f.stat_file)
		update_api_stats(rt, &start, http_code, retry);
	lprintf(LOG_INFO, ">>> API(out) (iReq:%lu) retry=%u json size=%lu\n"
			, i_API_call
			, retry
			, j->cb);

	return http_code;
}

static bool init_dir_done(struct dir *d, struct dentries **dent,
			  memory_order order)
{
	*dent = atomic_load_explicit(&d->dent, order);
	if (NULL == *dent)
		return false;
	return true;
}


static void init_dir(struct dir *d, struct dentries **dent)
{
	static const char ls_post[] = "{\"folder_id\":%lld,\"files\":1}";
	static const char ls_shared[] =
		"{\"folder_id\":0,\"sharing_user\":\"%s\",\"files\":1}";
	CURL *curl;
	long http_code;
	struct curl_slist *list = NULL;
	struct json j;

	if (init_dir_done(d, dent, memory_order_acquire))
		return;

	lprintf(LOG_DEBUG, "init_dir: `%s` (id=%"PRId64")\n"
			 , d->name, d->id);

	lock(&d->lookupmutex, NULL);
	/* In case another process simultaneously initialises the same dir
	 * we must now look again at pointers. Since we are just behind
	 * a lock relaxed is enough here.
	 */
	if (init_dir_done(d, dent, memory_order_relaxed)) {
		lprintf(LOG_DEBUG,"init_dir already done: `%s`\n", d->name);
		unlock(&d->lookupmutex, NULL);
		return;
	}
	curl = curl_init();
	json_init(&j);
	if (IS_SHARED_ROOT(d)) {
		char buf[sizeof(ls_shared) + strlen(d->name) ];
		snprintf(buf, sizeof(buf), ls_shared, SHARER_EMAIL(d));

		http_code = curl_json_perform(curl, &list, buf, &j,
					      FOLDER_LS, d->name);	
	}
	else {
		char buf[sizeof(ls_post) + STRLEN_MAX_INT64];
		snprintf(buf, sizeof(buf), ls_post, d->id);

		http_code = curl_json_perform(curl, &list, buf, &j,
					      FOLDER_LS, d->name);
	}
	parse_all(d,
		  (HTTP_OK == http_code) ? &j : NULL, dent);
	json_free(&j);
	if (0 != params1f.refresh_time && &root == d) {
		struct timespec	  now;
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		atomic_store_explicit(&refresh_start, now.tv_sec,
				      memory_order_relaxed);
	}
	unlock(&d->lookupmutex, NULL);

	curl_slist_free_all(list);
	curl_easy_cleanup(curl);

	if (LOG_DEBUG <= params.log_level) {
		char buf[32];
		char shared[] = { 'W', 'R', 'H', 'E' };
		char hint;
		int i;
		lprintf(LOG_DEBUG,
			"`%s` has %ld subdirs and %ld files\n",
			d->name,
			(*dent)->n_subdirs,
			(*dent)->n_files );
		for (i = 0; i< (*dent)->n_subdirs; i++) {
			ctime_r(&(*dent)->subdirs[i].cdate, buf);
			buf[24]='\0';
			if (ACCESS_RW == (*dent)->subdirs[i].access)
				hint = ' ';
			else
				hint = shared[(*dent)->subdirs[i].access &
					      ACCESS_MASK];
			lprintf(LOG_DEBUG, "%03d: [%c% 11ld|%s] %s\n"
					 , i + 1
					 , hint
					 , (*dent)->subdirs[i].id
					 , buf
					 , (*dent)->subdirs[i].name);
		}
		for (i = 0; i< (*dent)->n_files; i++) {
			ctime_r(&(*dent)->files[i].cdate, buf);
			buf[24]='\0';
			lprintf(LOG_DEBUG,"%03d: [% 12"PRId64"|%s] %s => %s\n"
					 , i + 1
					 , (*dent)->files[i].size
					 , buf
					 , (*dent)->files[i].filename
					 , (*dent)->files[i].URL);
		}
	}
}

static void free_cur_live_dump_new(struct streams *old, struct streams *new,
				   const char *where)
{
	struct rcu_free_p *head;
	unsigned long cnt;
	struct strm_loc *loc;
	unsigned long i;

	if (NULL != old) {
		head = fs_alloc(sizeof(struct rcu_free_p));
		head->p = old;
		call_rcu((struct rcu_head *)head, rcu_free_ptr);
		lprintf(LOG_DEBUG,
			">> %s: rcu file array pointer %p\n", where, old);
	}
	if (NULL == new) {
		lprintf(LOG_DEBUG, ">> %s: new stream list is now empty\n"
				 , where);
		return;
	}
	for (i=0; i < new->n_streams; i++) {
		cnt = atomic_load_explicit(&new->a_streams[i]->counter,
					   memory_order_acquire);
		loc = atomic_load_explicit(&new->a_streams[i]->loc,
					   memory_order_acquire);
		lprintf(LOG_DEBUG, ">> %s: new stream list[%lu] %s (%lX) >%lu\n"
				 , where
				 , i
				 , (IS_HIDDEN(new->a_streams[i]->access) ?
					new->a_streams[i]->path		 :
					new->a_streams[i]->URL
				   )
				 , cnt
				 , (NULL == loc) ? 0 : loc->until);
	}
}

static void free_stream(struct stream *s, const unsigned long counter)
{
	struct rcu_free_p *head;
	struct strm_loc *loc;

	loc = atomic_load_explicit(&s->loc, memory_order_acquire);
	lprintf(LOG_DEBUG, ">> RCU and free stream %p(->%p): %s (%lX).\n"
			 , s, loc, s->URL, counter);

	fs_free(loc);
	head = fs_alloc(sizeof(struct rcu_free_p));
	head->p = s;
	call_rcu((struct rcu_head *)head, rcu_free_ptr);
}

/* Available storage need not be refreshed for operations that
 * do not change it. According to 1fichier's answer, these are:
 * rename/move and create/delete directory. Hence in these operation
 * this function is called with the refresh_storage flag to false.
 *
 * Refresh also cleans potential streams with the ERROR flag on.
 */

static void refresh(struct dir *d, enum refresh_cause cause, bool ref_stor)
{
	unsigned long	spin = 0;
	struct dentries *dent;
	struct streams *cur, *new;
	unsigned long	i, j, counter;

	update_refresh_stats(cause);

	dent = atomic_fetch_and_explicit(&d->dent, 0, memory_order_acq_rel);
	if (NULL != dent) {
		struct rcu_free_d *head;
		lprintf(LOG_INFO, ">> Refreshing on %s %s=%p.\n"
				, refresh_msgs[cause], d->name, dent);
		head = fs_alloc(sizeof(struct rcu_free_d));
		head->dent = dent;
		call_rcu((struct rcu_head *)head, rcu_clean);
	}
	if (ref_stor) {
		lock(&storage_mutex, NULL);
		storage_flags |= FL_STOR_REFRESH;
		unlock(&storage_mutex, NULL);
	}

	cur = atomic_load_explicit(&live, memory_order_acquire);
	do {
		if (0 != spin)
			fs_free(new);
		spin++;
		if (NULL == cur)
			return;
		for (i = j = 0; i < cur->n_streams; i++) {
			counter = atomic_load_explicit(
						&cur->a_streams[i]->counter,
						memory_order_acquire);
			if (0 == (counter & OPEN_MASK) &&
			    0 != (counter & FL_ERROR))
				j++;
		}
		if (0 == j)
			return;
		new = fs_alloc(sizeof(struct streams) +
			       sizeof(void *) * (cur->n_streams - j));
		for (i = j = 0; i < cur->n_streams; i++) {
			counter = atomic_load_explicit(
						&cur->a_streams[i]->counter,
						memory_order_acquire);
			if (0 == (counter & OPEN_MASK) &&
			    0 != (counter & FL_ERROR))	   {
				/* When a stream is marked as error, the first
				 * refresh will delete it. Unlikely we have
				 * several refreshes in parallel in which case
				 * if FL_DEL is already marked, we just continue
				 * because it means another parallel refresh
				 * just did DEL/call_rcu
				 */
				if (unlikely(0 != (counter & FL_DEL)))
					continue;
				counter = atomic_fetch_or_explicit(
						&cur->a_streams[i]->counter,
						FL_DEL,
						memory_order_acq_rel);
				if (unlikely(0 != (counter & FL_DEL)))
					continue;
				free_stream(cur->a_streams[i], counter);
			}
			else {
				if (0 == (counter & FL_DEL))
					new->a_streams[j++] = cur->a_streams[i];
			}

		}
		/* Note that there could be more nodes in error that what we
		 * counting initially if error happen in parallel of refresh
		 * Hence we always alloc, and free if necessary.
		 */
		if (0 == j) {
			fs_free(new);
			new = NULL;
		}
		else {
			new->n_streams = j;
		}
	} while(!atomic_compare_exchange_strong_explicit(&live,
							 &cur,
							 new,
							 memory_order_acq_rel,
							 memory_order_acquire));
	spin_add(spin -1, "refresh");
	free_cur_live_dump_new(cur, new, "Refresh");
}

static int find_dir(const void *key,const void *dir)
{
	int res;
	const struct pbcb *k = (const struct pbcb *)key;
	const struct dir  *d = (const struct dir *)dir;

	res =  strncmp( k->pb, d->name, k->cb );
	if (0 != res)
		return res;
	if ('\0' == d->name[k->cb])
		return 0;
	else
		return -1;
}

static int find_file(const void *key,const void *file)
{
	int res;
	const struct pbcb *k = (const struct pbcb *)key;
	const struct file *f = (const struct file *)file;

	res =  strncmp( k->pb, f->filename, k->cb );
	if (0 != res)
		return res;
	if ('\0' == f->filename[k->cb])
		return 0;
	else
		return -1;
}

static void timed_refresh()
{
	struct timespec	  now;
	unsigned long	start;

	start = atomic_load_explicit(&refresh_start, memory_order_acquire);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
	if (now.tv_sec - start >= params1f.refresh_time &&
	    atomic_compare_exchange_strong_explicit(&refresh_start,
						    &start,
						    now.tv_sec,
						    memory_order_acq_rel,
						    memory_order_relaxed)
	   )
	/* If atomic exchange fails, it means another thread saw the
	 * the timeout and already refreshed, so we don't do it twice!
	 */
		refresh(&root, REFRESH_TIMER, true);
}

static void find_path(const char *path, struct walk *w)
{
	const char *p, *q;
	struct dir *d = &root;
	struct dentries *dent;
	struct pbcb key;

	lprintf(LOG_DEBUG, "find_path: `%s`\n", path);

	if ((NULL == params1f.refresh_file ||
	     0 != strcmp(path + 1, params1f.refresh_file)) &&
	     0 != params1f.refresh_time)
		timed_refresh();

	w->is_dir  = true;
	w->res.dir = NULL;
	w->parent  = NULL;
	if ('\0' == path[1]) { /* This is only true when path is '/' */
		w->res.dir = &root;
		w->parent  = &root;
		return;
	}
	for (p = path + 1; NULL != (q = strchr(p, '/')); p = q + 1) {
		key.pb = (char *)p;
		key.cb = q - p;
		init_dir(d, &dent);
		d = bsearch(&key, dent->subdirs, dent->n_subdirs
				, sizeof(struct dir), find_dir);
		if (NULL == d)
			return;
	}
	key.pb = (char *)p;
	key.cb = strlen(p);
	w->parent = d;
	init_dir(d, &dent);
	w->res.dir = bsearch(&key, dent->subdirs, dent->n_subdirs
				 , sizeof(struct dir), find_dir);
	if (NULL == w->res.dir) {
		w->is_dir = false;
		w->res.fs = bsearch(&key, dent->files, dent->n_files
					, sizeof(struct file), find_file);
		if (NULL != w->res.fs && F_DEL(w->res.fs))
			w->res.fs = NULL;
	}
}

static int unfichier_getattr_wrapped(const char *path, struct stat *stbuf)
{
	struct walk w;
	bool is_refresh_file;

	find_path(path, &w);

	if (NULL != params1f.refresh_file && &root == w.parent &&
	    0 == strcmp(path + 1, params1f.refresh_file))
		is_refresh_file = true;
	else
		is_refresh_file = false;

	if (NULL == w.res.fs) {
		if (params1f.refresh_hidden && is_refresh_file)
			refresh(&root, REFRESH_HIDDEN, true);
		memset(stbuf, 0, sizeof(struct stat));
		return -ENOENT;
	}
	memcpy(stbuf, &params.mount_st, sizeof(struct stat));
	if (w.is_dir) {
		stbuf->st_atim.tv_sec =
		stbuf->st_ctim.tv_sec =
		stbuf->st_mtim.tv_sec = w.res.dir->cdate;
		if (ACCESS_RW != (w.res.dir->access & ACCESS_MASK)) {
			stbuf->st_mode &= ~(S_IWUSR | S_IWGRP | S_IWOTH);
		}
	}
	else {
		stbuf->st_mode		= st_mode;
		stbuf->st_nlink		= 1;
		if (is_refresh_file) {
			stbuf->st_size	= 0;
			stbuf->st_blocks= 0;
		}
		else {
			stbuf->st_size	= w.res.fs->size;
			stbuf->st_blocks= w.res.fs->size / ST_BLK_SZ;
			stbuf->st_atim.tv_sec	=
			stbuf->st_ctim.tv_sec	=
			stbuf->st_mtim.tv_sec	= w.res.fs->cdate;
		}
	}
	return 0;
}

static int unfichier_getattr(const char *path, struct stat *stbuf)
{
	int res;

	lprintf(LOG_DEBUG,"getattr: `%s`\n", path);
	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_getattr_wrapped(path, stbuf);

	rcu_read_unlock();
	rcu_unregister_thread();
	return res;
}

static int unfichier_readdir_wrapped(const char *path, void *buf,
				     fuse_fill_dir_t filler, off_t offset,
				     struct fuse_file_info *fi)
{
	(void) offset;
	(void) fi;
	struct walk w;
	unsigned long i;
	struct dentries *dent;

	find_path(path, &w);
	if (NULL == w.res.dir || !w.is_dir)
		return -ENOENT;

	init_dir(w.res.dir, &dent);

	if (0 != filler(buf, ".", NULL, 0))
		return -EBADF;
	if (&root == w.res.dir) {
		if (0 != filler(buf, "..", NULL, 0))
			return -EBADF;
	}

	for (i = 0; i < dent->n_subdirs; i++)
		if ( 0 != filler(buf, dent->subdirs[i].name, NULL, 0))
			return -EBADF;
	for (i = 0; i < dent->n_files; i++)
		if (!F_DEL(&dent->files[i]))
			if (0 != filler(buf, dent->files[i].filename, NULL, 0))
				return -EBADF;
	return	0;
}

static int unfichier_readdir(const char *path, void *buf,
			     fuse_fill_dir_t filler, off_t offset,
			     struct fuse_file_info *fi)
{
	int res;

	lprintf(LOG_DEBUG,"readdir: `%s`\n", path);
	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_readdir_wrapped(path, buf, filler, offset, fi);

	rcu_read_unlock();
	rcu_unregister_thread();
	return res;
}

static int cmp_live(const void *URL,const void *p2)
{
	const struct stream *s = *(const struct stream **)p2;

	if (IS_HIDDEN(s->access))
		return 1;
	else
		return strcmp(URL, s->URL);
}
static int cmp_hidden(const void *path,const void *p2)
{
	const struct stream *s = *(const struct stream **)p2;

	if (IS_HIDDEN(s->access))
		return strcmp(path, s->path);
	else
		return -1;
}

/*
 * @brief manage list of opened files
 *
 * When a file not present in the list is opened, these functions create the new
 * array list, sorting out potential no longer needed files.
 *
 * Principle:
 *   The counter has 3 parts,
 *   - number of times the file is opened
 *   - a DEL flag
 *   - an ERROR flag
 * When the file is not needed anymore: not opened, not error, and not yet DEL,
 * (it means the whole counter is zero), then the algorithm tries to set the DEL
 * flag. If it succeeds (atomic_compare_exchange), then the memory is RCUed and
 * the now unneeded file is removed from the array.
 * Doing so ensures that only one thread will succeed in setting the DEL flag,
 * avoiding a double RCU that will trigger a double-free.
 * Also, if a thread wants to reuse a file while another one wants to delete it
 * the first doing the exchange will win, and the other thread will loop.
 *
 * The ERROR flag is not managed here, but during refresh. Files having had an
 * error are kept until refresh has been triggered, to avoid uselessly trying
 * to open them.
 */

static bool keep_live_stream(struct stream *s, struct timespec *t)
{
	unsigned long counter, spin = 0;
	struct strm_loc *loc;

	do {
		if (0 == spin)
			counter = atomic_load_explicit(&s->counter,
						       memory_order_acquire);
		spin++;

		if (0 != (counter & FL_ERROR) || 0 != (counter & ~3))
			return true;
		if (0 != (counter & FL_DEL))
			return false;
		/* From here: no error, not marked as del, and file not opened
		 *	      thus the only possible value of counter is: 0 */
		loc = atomic_load_explicit(&s->loc, memory_order_acquire);
		if (0 == t->tv_nsec)
			clock_gettime(CLOCK_MONOTONIC_COARSE, t);
		if (NULL != loc && loc->until > t->tv_sec)
			return true;
	 } while(!atomic_compare_exchange_strong_explicit(&s->counter,
						    &counter,
						    counter | FL_DEL,
						    memory_order_acq_rel,
						    memory_order_acquire));
	spin_add(spin - 1, "keep_live_stream");
	free_stream(s, counter);
	return false;
}

static int cmp_streams(const struct stream *s1, const struct stream *s2)
{
	if (IS_HIDDEN(s1->access)) {
		if (IS_HIDDEN(s2->access))
			return strcmp(s1->path, s2->path);
		else
			return -1;
	}
	else {
		if (IS_HIDDEN(s2->access))
			return 1;
		else
			return strcmp(s1->URL, s2->URL);
	}
}

static struct streams *new_streams_array(struct streams *cur,
					 struct stream	*added_stream,
					 struct timespec *t)
{
	struct streams *new;
	unsigned int i;
	bool ins = false;

	if (NULL == cur) {
		new = fs_alloc(sizeof(struct streams) + sizeof(void *));
		new->n_streams	  = 1;
		new->a_streams[0] = added_stream;
		return new;
	}

	/* We might allocated more memory than needed, but it is
	 * only pointers here, so no big deal
	 */
	new = fs_alloc(sizeof(struct streams) +
		       sizeof(void *) * (cur->n_streams + 1));
	new->n_streams = 0;
	for (i = 0; i < cur->n_streams; i++) {
		if (!keep_live_stream(cur->a_streams[i], t))
			continue;
		if (!ins && cmp_streams(added_stream, cur->a_streams[i]) <= 0) {
			ins = true;
			new->a_streams[new->n_streams++] = added_stream;
		}
		new->a_streams[new->n_streams++] = cur->a_streams[i];
	}

	if (!ins)
		new->a_streams[new->n_streams++] = added_stream;

	return new;
}

static struct stream *find_live_stream(const char *path,
				       struct walk *w,
				       struct streams *live,
				       unsigned long *counter)
{
	struct stream **found;

	if (NULL == live) {
		/* This situation happens only the first time a file is
		 * opened. Due to parallelislm, the message can possibly
		 * be displayed several times. */
		lprintf(LOG_DEBUG, "Open: first opened file(s).\n");
		return NULL;
	}

	if (IS_HIDDEN(w->parent->access))
		found = bsearch(path, live->a_streams, live->n_streams,
				sizeof(void *), cmp_hidden);
	else
		found = bsearch(w->res.fs->URL, live->a_streams,
				live->n_streams, sizeof(void *), cmp_live);
	if (NULL == found)
		return NULL;

	*counter = atomic_load_explicit(&(*found)->counter,
					memory_order_acquire);
	if (0 != ((*counter) & FL_DEL))
		return NULL;
	return *found;
}

static struct stream *new_stream(struct file *f, struct timespec *t,
				 const char * path, struct dir *parent)
{
	struct stream *new_stream = NULL;
	
	if (0 == t->tv_nsec)
		clock_gettime(CLOCK_MONOTONIC_COARSE, t);
	if (params1f.refresh_file == f->URL) {
		new_stream = fs_alloc(sizeof(struct stream));
		new_stream->URL = params1f.refresh_file;
	}
	else {
		size_t sz1;
		if (IS_HIDDEN(parent->access))
			sz1 = strlen(f->filename)  + 
			      strlen(parent->name) - (parent->access >> 2) + 2;
		else
			sz1 = strlen(f->URL) + 1;
		new_stream = fs_alloc(sizeof(struct stream) + sz1
							    + strlen(path) + 1);
		strcpy(new_stream->path, path);
		new_stream->URL = (char *)(new_stream->path + strlen(path) + 1);

		if (IS_HIDDEN(parent->access)) {
			strcpy(new_stream->URL, f->filename);
			new_stream->email = new_stream->URL +
					    strlen(f->filename) + 1;
			strcpy(new_stream->email,
			       parent->name + (parent->access >> 2));
			new_stream->id = parent->id;
		}
		else {
			strcpy(new_stream->URL, f->URL);
		}
	}
	atomic_store_explicit(&new_stream->loc,
			      NULL,
			      memory_order_release);
	atomic_store_explicit(&new_stream->counter,
			      OPEN_INC,
			      memory_order_release);
	new_stream->access 	= parent->access;
	new_stream->size	= f->size;
	new_stream->last_rq_id	= 0;
	new_stream->nb_loc      = 0;
	atomic_store_explicit(&new_stream->nb_streams,
			      0,
			      memory_order_release);
	pthread_mutex_init(&new_stream->fastmutex, NULL);
	pthread_mutex_init(&new_stream->curlmutex, NULL);

	return new_stream;
}


/*
 * @brief does the actual work for opening files
 *
 * Opened files and files that have a still valid location are accessible
 *   through the global atomic pointer 'opened'.
 * This points to a structure that has an array of pointers to those opened
 *   files. This is desirable, because even when a file have been closed, the
 *   'download ticket' (location) can still be valid. Thus it saves the time
 *   to get a new download ticket if the file is opened again in the validity
 *   period. This can't be handled like astreamfs because the directory tree
 *   can be flushed, and storing the location in the tree means it can be
 *   lost when refreshed, resulting in sub-optimal behavior.
 *   This also saves memory when we have a lot of files and few at a time are
 *   opened.
 *
 * This global list is managed without mutexes but with an atomic exchanges.
 * It could suffer from live lock in situation where the driver would be bombed
 * with parallel opens, but is a conscious choice because such a situation
 * is probably not a standard use!
 * See discussions about pro and cons of live-locks/spin-locks vs mutexes.
 *
 * All the management of the list is done at open. Release (close) only
 * decrements the counter (number of times the files is opened).
 * Refresh takes care of removing files with errors.
 *
 * @param path of the file to be opened (in)
 * @param address of the file information to return if succesful (out)
 * @return 0 if Ok or error code (negative)
 */

static int unfichier_open_wrapped(const char *path, struct fuse_file_info *fi)
{
	struct walk w;
	unsigned long spin1 = 0, spin2;
	unsigned long counter;
	struct timespec t;
	struct streams *live_cur, *live_new = NULL;
	struct stream  *stream_found, *stream_new = NULL;

	find_path(path, &w);
	if (NULL == w.res.fs || w.is_dir)
		return -ENOENT;

	if ((fi->flags & 3) != O_RDONLY)
		return -EACCES;

	lprintf(LOG_DEBUG, ">> Open: file path is: %s.\n", path);

	if (params1f.refresh_file == w.res.fs->filename) {
		refresh(&root, REFRESH_TRIGGER, true);
		fi->fh = (uintptr_t)params1f.refresh_file;
		return 0;
	}
	if (params1f.stat_file == w.res.fs->filename) {
		fi->fh = (uintptr_t)out_stats(NULL);
		return 0;
	}

	do {
		t.tv_nsec = 0;
		stream_found = NULL;
		if (0 == spin1) {
			/* First time we need to atomically load 'live'.
			 * Subsequent times, it is done with compare_exchange */
			live_cur = atomic_load_explicit(&live,
						  memory_order_acquire);
		}
		else {
			fs_free(live_new);
			live_new = NULL;
		}
		spin1++;
		spin2 = 0;
		do {
			if (0 == spin2)
				stream_found = find_live_stream(path,
								&w,
								live_cur,
								&counter);
			spin2++;
			if (NULL != stream_found) {
				fs_free(stream_new);
				if (0 != (counter & FL_ERROR)) {
					fi->fh = (uintptr_t)stream_found;
					return -EACCES;
				}
				lprintf(LOG_DEBUG,
					">> Open: file found: %s.\n", path);

			}
		} while(NULL != stream_found &&
			!atomic_compare_exchange_strong_explicit(
						    &stream_found->counter,
						    &counter,
						    counter + OPEN_INC,
						    memory_order_acq_rel,
						    memory_order_acquire));
		spin_add(spin2 - 1, "unfichier_open_wrapped(1)");
		if (NULL != stream_found) {
			stream_new = stream_found;
			break;
		}

		if (NULL ==  stream_new)
			stream_new = new_stream(w.res.fs, &t, path, w.parent);

		live_new = new_streams_array(live_cur, stream_new, &t);
	} while(!atomic_compare_exchange_strong_explicit(&live,
							 &live_cur,
							 live_new,
							 memory_order_acq_rel,
							 memory_order_acquire));
	spin_add(spin1 - 1, "unfichier_open_wrapped(2)");
	if (NULL != live_new)
		free_cur_live_dump_new(live_cur, live_new, "Open");

	stream_new->no_ssl = false;
	for (counter = 0; counter < params1f.n_no_ssl; counter++)
		if (0 == strncmp(path, params1f.a_no_ssl[counter].pb
				     , params1f.a_no_ssl[counter].cb)) {
			stream_new->no_ssl = true;
			break;
		}
	
	lprintf(LOG_DEBUG, ">> Open: stream handle: %p.\n", stream_new);
	fi->fh = (uintptr_t)stream_new;
	return 0;
}

/*
 * @brief fuse callback for open
 *
 * @param path of the file to be opened (in)
 * @param address of the file information to return if succesful (out)
 * @return 0 if Ok or error code (negative)
 */

static int unfichier_open(const char *path, struct fuse_file_info *fi)
{
	int res;

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_open_wrapped(path, fi);

	rcu_read_unlock();
	rcu_unregister_thread();
	return res;
}


/*
 * @brief fuse callback for release
 *
 * Close instruction to async reader are only sent if the file is not opened
 * anymore and there was an error. Otherwise the stream is left open in case
 * it is shortly needed again. This also avoid having to lock here and in open
 * because a file marked as error cannot be re-opened with the same handle.
 *
 * @param path of the file to be opened (in)
 * @param address of the file information to return if succesful (out)
 * @return 0 if Ok or error code (negative)
 */

static int unfichier_release(const char *path, struct fuse_file_info *fi)
{
	struct stream *s;
	unsigned long nb_open;

	s = (struct stream *)((uintptr_t)(fi->fh));

	/* This is the constant address we have for refresh */
	if (s == (struct stream *)params1f.refresh_file) {
		lprintf(LOG_INFO, ">> Releasing trigger file.\n");
		return 0;
	}
	/* Stats: 1 open/1 buffer, so here we just need to free it */
	if (0 != (((struct stats *)((uintptr_t)(fi->fh)))->counter & FL_SPECIAL)) {
		lprintf(LOG_DEBUG, ">> Releasing stat file.\n");
		fs_free((void *)(uintptr_t)(fi->fh));
		return 0;
	}
	rcu_register_thread();
	rcu_read_lock();

	nb_open = atomic_fetch_sub_explicit(&s->counter, OPEN_INC,
					    memory_order_acq_rel);

	lprintf(LOG_INFO, ">> Releasing %s (%ld).\n", path, nb_open);
	work.size = 0;

	rcu_read_unlock();
	rcu_unregister_thread();
	return 0;
}

static bool ok_json_get(const char *json_str, const char *pattern, off_t *value)
{
	char *p;
	p = json_find_key(json_str, pattern);
	if (NULL == p) {
		lprintf(LOG_ERR,
			"Unexpected/Ignoring: did not find %s key in json response (URL=%s)\n",
			pattern, api_urls[USER_INFO]);
		return false;
	}
	if ( 1 != sscanf(p, "%"SCNd64, value) ) {
		lprintf(LOG_ERR,
			"Unexpected/Ignoring: could not read the value of %s (URL=%s)\n",
			pattern, api_urls[USER_INFO]);
		return false;
	}
	return true;
}

#define TERA_BYTE (1024ULL * 1024 * 1024 * 1024)

static int extract_avail(long http_code, const char *json_str)
{
	off_t hot, cold, allowed, x;
	if (HTTP_OK != http_code)
		return -EIO;
	if (!ok_json_get(json_str, json_cold_storage, &cold))
		return -EIO;
	if (!ok_json_get(json_str, json_hot_storage, &hot))
		return -EIO;
	if (!ok_json_get(json_str, json_allowed_cold_storage, &allowed))
		return -EIO;
	lprintf(LOG_DEBUG,
		"<<< unfichier_statfs cold=%llu, hot=%llu, allowed=%llu\n",
		cold, hot, allowed);
	allowed *= TERA_BYTE;
	x = (allowed + hot + DEFAULT_BLK_SIZE - 1) / DEFAULT_BLK_SIZE;
	max_storage = x;
	if (cold >= allowed)
		x = 0;
	else
		x = (allowed - cold) / DEFAULT_BLK_SIZE;
	avail_storage = x;
	return 0;
}

static int unfichier_statfs(const char *path, struct statvfs *buf)
{
	struct timespec	now;
	int res;
	bool refresh = false;

	/* Note: this function does NOT use possibly dangling pointer.
	 *	 It only uses static root (if refresh is needed) and
	 *	 static available_storage and the like.
	 *	 Hence it does not need rcu_lock, etc...
	 */

	lock(&storage_mutex, NULL);

	if (0 != storage_flags) {
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		if (0 == storage_next_update ||
		    now.tv_sec > storage_next_update) {
			storage_next_update = now.tv_sec + USER_INFO_DELAY;
			refresh = true;
		}
	}
	if (refresh) {
		static const char empty_json_object[]="{}";
		CURL *curl;
		long http_code;
		struct curl_slist *list = NULL;
		struct json j;

		lprintf(LOG_DEBUG, "<<<(in) unfichier_statfs\n");

		json_init(&j);
		curl = curl_init();
		http_code = curl_json_perform(curl,
					      &list,
					      (char *)empty_json_object,
					      &j,
					      USER_INFO,
					      (char *)path);
		curl_slist_free_all(list);
		curl_easy_cleanup(curl);
		res = extract_avail(http_code, j.pb);
		storage_flags = (0 == res) ? 0 : FL_STOR_ERROR;
		json_free(&j);
	}
	else {
		lprintf(LOG_DEBUG, "<<<(in/out)>>> unfichier_statfs\n");
		res = (0 == (FL_STOR_ERROR & storage_flags)) ? 0 : -EIO;
	}
	buf->f_blocks = max_storage;
	buf->f_bfree  =
	buf->f_bavail = avail_storage;

	unlock(&storage_mutex, NULL);

	buf->f_bsize  = DEFAULT_BLK_SIZE;
	buf->f_frsize = DEFAULT_BLK_SIZE;

	return res;
}

static bool forbidden_change(struct walk *w)
{
	if (w->is_dir && 0 != (w->res.dir->access & ~ACCESS_MASK)) {
		lprintf(LOG_WARNING,
			"cannot rename or remove the shared directory: `%s`.\n",
			w->res.dir->name);
		return true;
	}
	return false;
}

static bool forbidden_write(struct dir * d)
{
	if (ACCESS_RW != (d->access & ACCESS_MASK)) {
		lprintf(LOG_WARNING,
			"no write access on shared directory: `%s`.\n",
			d->name);
		return true;
	}
	return false;
}

static bool forbidden_operation(struct dir * d)
{
	if (ACCESS_HIDDEN == (d->access & ACCESS_MASK)) {
		lprintf(LOG_WARNING,
			"impossible operation with 'hidden links' shared directory: `%s`.\n",
			d->name);
		return true;
	}
	return false;
}

static bool forbidden_path(const char* path, const char *msg)
{
	if (NULL != params1f.refresh_file &&
	    0 == strcmp(path + 1, params1f.refresh_file)) {
		lprintf(LOG_WARNING,
			"impossible to %s the refresh_file.\n",
			msg);
	    return true;
	}
	if (NULL != params1f.stat_file &&
	    0 == strcmp(path + 1, params1f.stat_file)) {	
		lprintf(LOG_WARNING,
			"impossible to %s the stat_file.\n",
			msg);
	    return true;
	}
	return false;
}


/* Note: see man 2 rename
 *  rename is always called with either 2 files (since we don't have
 *  symlinks in the mount), or with a dir as oldpath (from), in which case
 *  newpath (to) is either non existing or an empty dir.
 *
 *  In this version of fuse (26) we do not have the 'flags' of renameat2
 *  Since anyway it is impossible to do "atomic exchange" talking to a server
 *  that does not, the code has the behaviour of RENAME_NOREPLACE flag.
 *  This means that when newpath (to) exists, the return code is EEXIST.
 *  Should the user want to rename replacing an existing file, he shall
 *  first delete the target, then do the rename.
 *  rename also documents that in case of 2 hardlinks referring to the same
 *  content, it does nothing. To avoid having to call the server to check that,
 *  the code does not do that, instead it will also return EEXIST in this case.
 */

static int unfichier_rename_wrapped(CURL *curl, struct curl_slist **list,
				    const char *from, const char *to)
{
	static const char chattr_post[]	 = "{\"urls\":[\"%s\"],\"filename\":\"%s\"}";
	static const char mv_file_post[] = "{\"urls\":[\"%s\"],\"destination_folder_id\":%lld,\"rename\":\"%s\"}";
	static const char mv_fusr_post[] = "{\"urls\":[\"%s\"],\"destination_folder_id\":0,\"destination_user\":\"%s\",\"rename\":\"%s\"}";
	static const char mv_dir_post[]	 = "{\"folder_id\":%lld,\"destination_folder_id\":%lld,\"rename\":\"%s\"}";
	static const char mv_dusr_post[] = "{\"folder_id\":%lld,\"destination_folder_id\":0,\"destination_user\":\"%s\",\"rename\":\"%s\"}";
	long http_code = HTTP_OK;
	struct walk wf, wt;
	char *p, *q, *r = NULL;
	struct json j;
	json_init(&j);

	/* Note that Linux kernel (normally!) does NOT call the fuse driver in
	 * error situations like renaming of different types: file to dir, dir
	 * to file or dir to subdir, component of paths does not exist.
	 * We do the test anyway... better safe than sorry... but this error
	 * test code should never be reached.
	 * So we should never return (on Linux): EINVAL, EISDIR, ENOTDIR.
	 * Since we do in fact the same as renameat2(RENAME_NOREPLACE), kernel
	 * DOES call this function when target exists. Thus the only test that
	 * is hit before calling the move APIs is EEXIST
	 * When calling the move APIs we can have ENOENT (file/dir removed from
	 * server) or EREMOTEIO (other error on server).
	 * EACCESS is also returned when trying to play with the refresh_file,
	 * or when doing an impossible operation with shares.
	 */

	find_path(from, &wf);
	if (NULL == wf.res.fs)
		return -ENOENT;
	if (forbidden_write(wf.parent) || forbidden_change(&wf))
		return -EACCES;
	/* That works only whent both paths are canonical. So for now...
	 * trust the kernel for not calling us on this situation. The correct
	 * test would be to do a loop on parents.
	size_t l;
	l = strlen(from);
	if (wf.is_dir && 0 == strncmp(from, to, l) && '/' == to[l])
		return -EINVAL;		*//* Renaming a dir to its children. */
	find_path(to, &wt);
	if (NULL != wt.res.fs) {
		if (wt.is_dir && !wf.is_dir)
			return -EISDIR;	/* Renaming a file to existing dir */
		if (!wt.is_dir && wf.is_dir)
			return -ENOTDIR;/* Renaming a dir to existing file */
		return -EEXIST;		/* Same type but already exists */
	}
	if (NULL == wt.parent)
		return -ENOTDIR;	/* Dir component of path not found */
	if (forbidden_write(wt.parent))
		return -EACCES;

	/* Directories */
	q = strrchr(to, '/');
	if (wf.is_dir) {
		if (IS_SHARED_ROOT(wt.parent)) {
			char post[sizeof(mv_dusr_post)+ STRLEN_MAX_INT64
						      + strlen(wt.parent->name)
						      + strlen(q + 1) ];
			sprintf(post, mv_dusr_post
				    , wf.res.dir->id
				    , SHARER_EMAIL(wt.parent)
				    , q+1);
			http_code = curl_json_perform(curl, list, post, &j,
						      FOLDER_MV, from);
		}
		else {
			char post[sizeof(mv_dir_post) + 2 * STRLEN_MAX_INT64
						      + strlen(q + 1) ];
			sprintf(post, mv_dir_post
				    , wf.res.dir->id
				    , wt.parent->id
				    , q+1);
			http_code = curl_json_perform(curl, list, post, &j,
						      FOLDER_MV, from);
		}
		json_free(&j);
		refresh(wf.parent, REFRESH_MV, false);
		refresh(wt.parent, REFRESH_MV, false);
		switch (http_code) {
			case HTTP_OK	    : return 0;
					/* Probably delete in parallel? */
			case HTTP_NOT_FOUND : return -ENOENT;
			default		    : return -EREMOTEIO;
		}
	}

	/* Files */
	p = strrchr(from, '/');
	if (p -	 from == q - to && 0 == strncmp(from, to, p - from))
	{
		char post[sizeof(chattr_post) + strlen(wf.res.fs->URL)
					      + strlen(q + 1)];
		sprintf(post, chattr_post, wf.res.fs->URL, q + 1);
		http_code = curl_json_perform(curl, list, post, &j,
					      FILE_CHATTR, from);
	}
	else {
		if (IS_SHARED_ROOT(wt.parent)) {
			char post[sizeof(mv_fusr_post) + strlen(wf.res.fs->URL)
						       + strlen(wt.parent->name)
						       + strlen(q + 1) ];
			sprintf(post, mv_fusr_post
				    , wf.res.fs->URL
				    , SHARER_EMAIL(wt.parent)
				    , q+1);
			http_code = curl_json_perform(curl, list, post, &j,
					      FILE_MV, from);
		}
		else {
			char post[sizeof(mv_file_post)	+ strlen(wf.res.fs->URL)
							+ STRLEN_MAX_INT64
							+ strlen(q + 1) ];
			sprintf(post, mv_file_post
				    , wf.res.fs->URL
				    , wt.parent->id
				    , q+1);
			http_code = curl_json_perform(curl, list, post, &j,
					      FILE_MV, from);
		}
	}
	if (HTTP_OK == http_code)
		r = strstr(j.pb, wf.res.fs->URL);
	json_free(&j);

	refresh(wt.parent, REFRESH_MV, false);

	if (NULL == r) {
		refresh(wf.parent, REFRESH_MV, false);
		if (HTTP_NOT_FOUND == http_code)
			return -ENOENT;	 /* Probably delete in parallel? */
		else
			return -EREMOTEIO;
	}
	else {
		if (wf.parent != wt.parent)
			atomic_fetch_or_explicit(&wf.res.fs->flags,
						 FL_DEL,
						 memory_order_acq_rel);
	}
	return 0;
}

static int unfichier_rename(const char *from, const char *to)
{
	int res;
	CURL *curl;
	struct curl_slist *list = NULL;
	char *p;
	static const char msg_rename[] = "rename";

	if (forbidden_path(from, msg_rename))
		return -EACCES;

	lprintf(LOG_DEBUG, "<<<(in) unfichier_rename `%s` to `%s`\n"
			 , from, to);

	rcu_register_thread();
	rcu_read_lock();

	curl = curl_init();
	p = json_escape(to);
	res = unfichier_rename_wrapped(curl, &list, from, p);
	if (NULL != list)
		curl_slist_free_all(list);
	if (p != to)
		fs_free(p);
	curl_easy_cleanup(curl);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_rename `%s` to `%s` res=%d\n"
			  , from, to, res);
	return res;
}

static int unfichier_link_wrapped(CURL *curl, struct curl_slist **list,
				  const char *source, const char *link)
{
	static const char cp_post[] = "{\"urls\":[\"%s\"],\"folder_id\":%lld,\"rename\":\"%s\"}";
	static const char cu_post[] = "{\"urls\":[\"%s\"],\"folder_id\":0,\"sharing_user\":\"%s\",\"rename\":\"%s\"}";
	long http_code = HTTP_OK;
	struct walk ws, wl;
	char *p;

	find_path(source, &ws);
	if (NULL == ws.res.fs || ws.is_dir)
		return -ENOENT;
	/* The API works only on files: -ENOSYS on directories
	 */
	if (ws.is_dir)
		return -ENOSYS;
	if (forbidden_operation(ws.parent))
		return -EACCES;
	find_path(link, &wl);
	if (NULL == wl.parent)
		return -ENOTDIR;
	if (NULL != wl.res.fs || wl.is_dir)
		return -EEXIST;
	if (forbidden_write(wl.parent))
		return -EACCES;

	p = strrchr(link  , '/');
	{
		struct json j;
		json_init(&j);
		if (IS_SHARED_ROOT(wl.parent)){
			char post[sizeof(cu_post) + strlen(ws.res.fs->URL)
						  + strlen(wl.parent->name)
						  + strlen(p + 1) ];
			sprintf(post, cu_post
				    , ws.res.fs->URL
				    , SHARER_EMAIL(wl.parent)
				    , p + 1);
			http_code = curl_json_perform(curl, list, post, &j
							  , FILE_CP, source);
		}
		else {
			char post[sizeof(cp_post) + strlen(ws.res.fs->URL)
						  + STRLEN_MAX_INT64
						  + strlen(p + 1) ];
			sprintf(post, cp_post
				    , ws.res.fs->URL
				    , wl.parent->id
				    , p + 1);
			http_code = curl_json_perform(curl, list, post, &j
							  , FILE_CP, source);
		}
		if (HTTP_OK == http_code)
			p = strstr(j.pb, ws.res.fs->URL);
		json_free(&j);
	}
	refresh(wl.parent, REFRESH_LINK, true);
	if (HTTP_OK != http_code || NULL == p) {
		/* Linking does not change source dir: refresh it on error only.
		 * If source = link's target, this refresh does nothing since
		 * we just refreshed the link's target directory above.
		 */
		refresh(ws.parent, REFRESH_LINK, false);
		if (HTTP_NOT_FOUND == http_code)
			return -ENOENT;	 /* Probably parallel delete? */
		else
			return -EREMOTEIO;
	}
	return 0;
}

static int unfichier_link(const char *source, const char *link)
{
	int res = 0;
	CURL *curl;
	struct curl_slist *list = NULL;
	char *l;
	static const char msg_link[] = "link from";

	if (forbidden_path(source, msg_link))
		return -EACCES;

	lprintf(LOG_DEBUG, "<<<(in) unfichier_link `%s` -> `%s`\n"
			 , source, link);

	rcu_register_thread();
	rcu_read_lock();

	curl = curl_init();
	l = json_escape(link);
	res = unfichier_link_wrapped(curl, &list, source, l);
	if (NULL != list)
		curl_slist_free_all(list);
	if (l != link)
		fs_free(l);
	curl_easy_cleanup(curl);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_link `%s` -> `%s` res=%d\n"
			  , source, link, res);
	return res;
}

static int unfichier_unlink_wrapped(const char *filename)
{
	static const char rm_post[] = "{\"files\":[{\"url\":\"%s\"}]}";
	long http_code = 0;
	char *p = NULL;
	CURL *curl;
	struct curl_slist *list = NULL;
	struct walk w;

	find_path(filename, &w);
	if (NULL == w.res.fs || w.is_dir)
		return -ENOENT;
	if (forbidden_write(w.parent))
		return -EACCES;
	{
		char post[sizeof(rm_post) + strlen(w.res.fs->URL)];
		struct json j;
		json_init(&j);

		sprintf(post, rm_post, w.res.fs->URL);
		curl = curl_init();
		http_code = curl_json_perform(curl, &list, post, &j,
					      FILE_RM, filename);
		curl_slist_free_all(list);
		curl_easy_cleanup(curl);
		if (HTTP_OK == http_code)
			p = strstr(j.pb, w.res.fs->URL);
		json_free(&j);
	}

	if (NULL == p) {
		refresh(w.parent, REFRESH_UNLINK, true);
		if (HTTP_NOT_FOUND == http_code)
			return -ENOENT;	 /* Probably deleted in parallel! */
		else
			return -EREMOTEIO;
	}
	else {
		atomic_fetch_or_explicit(&w.res.fs->flags,
					 FL_DEL,
					 memory_order_acq_rel);
	}

	return 0;
}

static int unfichier_unlink(const char *filename)
{
	int res;
	static const char msg_unlink[] = "unlink";

	if (forbidden_path(filename, msg_unlink))
		return -EACCES;

	lprintf(LOG_DEBUG, "<<<(in) unfichier_unlink `%s`\n", filename);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_unlink_wrapped (filename);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_unlink `%s` res=%d\n"
			  , filename, res);
	return res;
}

static int unfichier_rmdir(const char *dirname)
{
	struct walk w;
	int res;

	lprintf(LOG_DEBUG, "<<<(in) unfichier_rmdir `%s`\n", dirname);

	/* We cannot rmdir the root of the mount!
	 * Not needed, if we rmdir the mount, kernel says it is busy, so
	 * we never receive "/" as a dirname
	if ('/' == dirname[0] && '\0' == dirname[1])
		return -EACCES;
	*/

	rcu_register_thread();
	rcu_read_lock();

	find_path(dirname, &w);
	if (NULL == w.res.dir || !w.is_dir) {
		res = -ENOENT;
	}
	else {
		struct dentries *dent;
		unsigned long i;
		bool has_files = false;

		if (forbidden_write(w.parent) || forbidden_change(&w)) {
			res = -EACCES;
		}
		else {
			init_dir(w.res.dir, &dent);
			for (i = 0; i < dent->n_files; i++)
				if (!F_DEL(&dent->files[i])) {
					has_files = true;
					break;
				}
			if (0 != dent->n_subdirs || has_files) {
				res = -ENOTEMPTY;
			}
			else {
				static const char rmdir_post[] = "{\"folder_id\":%lld}";
				long http_code = 0;
				CURL *curl;
				struct curl_slist *list = NULL;
				char post[sizeof(rmdir_post) + STRLEN_MAX_INT64];
				struct json j;
				json_init(&j);

				sprintf(post, rmdir_post, w.res.dir->id);
				curl = curl_init();
				http_code = curl_json_perform(curl, &list, post,
							      &j, FOLDER_RM,
							      dirname);
				curl_slist_free_all(list);
				curl_easy_cleanup(curl);
				json_free(&j);

				refresh(w.parent, REFRESH_RMDIR, false);
				if (HTTP_NOT_FOUND == http_code) {
					res = -ENOENT;	/* Parallel delete? */
				}
				else {
					if (HTTP_OK == http_code)
						res = 0;
					else
						res = -EREMOTEIO;
				}
			}
		}
	}

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_rmdir `%s` res=%d\n"
			  , dirname, res);
	return res;
}

static int unfichier_mkdir_wrapped(const char *dirname)
{
	int	    res;
	struct walk w;
	const char *p;
	struct dir *parent;

	p =  strrchr(dirname, '/');
	if (p == dirname) { /* This is only for creation in root */
		parent = &root;
	}
	else
	{
		char parent_name[p - dirname + 1];
		memcpy(parent_name, dirname, p - dirname);
		parent_name[p - dirname] = '\0';
		find_path(parent_name, &w);
		if (NULL == w.res.dir || !w.is_dir)
			return -ENOENT;
		parent = w.res.dir;
	}

	find_path(dirname, &w);
	if (forbidden_write(w.parent))
		return -EACCES;
	if (NULL != w.res.dir) {
		return -EEXIST;
	}
	else {
		static const char mkdir_post[]= "{\"name\":\"%s\",\"folder_id\":%lld}";
		long http_code = 0;
		CURL *curl;
		struct curl_slist *list = NULL;
		char post[sizeof(mkdir_post) + strlen(p) + STRLEN_MAX_INT64];
		struct json j;
		json_init(&j);

		sprintf(post, mkdir_post, p + 1, parent->id);
		curl = curl_init();
		http_code = curl_json_perform(curl, &list, post, &j,
					      FOLDER_MKDIR,
					      dirname);
		curl_slist_free_all(list);
		curl_easy_cleanup(curl);
		json_free(&j);

		res = (HTTP_OK == http_code) ? 0 : -EREMOTEIO;
		refresh(parent, REFRESH_MKDIR, false);
	}
	return res;
}

static int unfichier_mkdir(const char *dirname, mode_t unused)
{
	(void)unused;
	int res;

	lprintf(LOG_DEBUG, "<<<(in) unfichier_mkdir `%s`\n", dirname);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_mkdir_wrapped(dirname);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_mkdir `%s` res=%d\n"
			  , dirname, res);
	return res;
}

/*
 * ***************************************************************************
 * From there starts the "stream engine" code
 */

static void get_work(struct reader *r)
{
	int err;
	struct timespec until;
	struct node *tmp, *rcv;


	if (r->idle || NULL != r->rcv) {
		lprintf(LOG_DEBUG,
			"get_work[%d] sem_wait\n", r - readers);
		err= sem_wait(&r->sem_go);
	}
	else {
		clock_gettime(CLOCK_REALTIME, &until);
		until.tv_sec += CURL_IDLE_TIME;
		lprintf(LOG_DEBUG,
			"get_work[%d] timedsem\n", r - readers);
		err= sem_timedwait(&r->sem_go, &until);
		if ( ETIMEDOUT == errno ) {
			lprintf(LOG_DEBUG,
				"get_work[%d]  timedout\n",
				r - readers);
			return;
		}
	}
	if ( 0 != err ) {
		lprintf(LOG_CRIT,
			"get_work[%d] sem_wait error: %d.\n",
			r - readers, err);
	}

	msg_pop( &r->rcv, &rcv );
	lprintf(LOG_DEBUG,
		"get_work[%d] got: %p\n",
		r - readers, rcv);
	while(1) {
		lprintf(LOG_DEBUG,
			"get_work[%d] poped: %p->%p %"PRIu64":%lu\n",
			r - readers, rcv, rcv->next, rcv->offset, rcv->size);
		tmp = rcv->next;
		if (0 == rcv->size) {
			rcv->next = r->prv;
			r->prv = rcv;
			if ( NULL != tmp ) {
				lprintf(LOG_CRIT,
					"get_work[%d] there should NOT be another request after close: %p %"PRIu64":%lu-%lu\n",
					r - readers, tmp, rcv->offset,
					rcv->written, rcv->size);

			}
			break;
		}
		else {
			inter_fill(r, rcv);
			prv_insert(&r->prv, rcv, &work);
			if ( NULL == tmp )
				break;
		}
		lprintf(LOG_DEBUG,
			"get_work[%d] additional sem_wait, rcv=%p\n",
			r - readers, tmp);
		sem_wait(&r->sem_go);
		rcv = tmp;
	}
}

#include "astream_engine.c"

struct s_loc {
	struct stream *s;
	CURL *curl;
	long  http_code;
};


static char content_range_header[]="Content-Range:";
#define S_CONTENT_RANGE_HEADER sizeof(content_range_header) -1

static size_t header_check(char *buffer, size_t size,
			   size_t nitems, struct s_loc *sl)
{
	struct stream *s = sl->s;
	char *p;
	size_t sz = nitems * size;
	unsigned long long file_size;

	/*
	 * Status code must be 206. For other codes we stop.
	 * Since first line of response is always the response code we can
	 * use curl_easy_getinfo directly: it is more portable (http/2, etc...)
	 */
	if ( 0 == sl->http_code ) {
		CURL_EASY_GETINFO(sl->curl, CURLINFO_RESPONSE_CODE,
				  &sl->http_code);
		switch (sl->http_code)
		{
			case HTTP_PARTIAL_CONTENT : 	return sz;
			case HTTP_NOT_FOUND :
			case HTTP_GONE	    :		return 0;
			default : lprintf(LOG_ERR,
					  "unexpected range request status code: %ld\n",
					   sl->http_code);
		}
	}


	/*
	 * Check the file size from the Content-Range header.
	 */
	else {
		if (sz <= S_CONTENT_RANGE_HEADER ||
		    0 != strncasecmp(buffer, content_range_header
					   , S_CONTENT_RANGE_HEADER))
			return sz;

		p= memchr(buffer + S_CONTENT_RANGE_HEADER, '/', sz);
		if (NULL != p) {
			file_size= 0;
			for (p++; p < buffer + sz && isdigit(*p) ; p++)
				file_size= 10 * file_size + *p - '0';
			if (s->size == file_size)
				return sz;
			lprintf(LOG_ERR,
				"wrong file size:%"PRIu64"/%"PRIu64"\n",
				file_size, s->size);
		}
		else {
			lprintf(LOG_ERR,
				"malformed Content-Range header: %.*s.\n",
				buffer, s);
		}
	}
	atomic_fetch_or_explicit(&s->counter, FL_ERROR, memory_order_acq_rel);
	return 0;
}




/*
 * @brief stores location-related informations.
 *
 * @param pointer on the current stream
 * @param location string
 * @param size of the location string (strlen, ie not counting '\0')
 * @param time of storage
 * @return none
 */
static const char *store_loc(struct stream *s,
			     struct strm_loc **loc,
			     const char	   *location ,
			     const size_t   sz_location,
			     const time_t   now_sec)
{
	(*loc) = fs_alloc(sizeof(struct strm_loc) + sz_location + 1);
	s->nb_loc ++;
	(*loc)->i_loc = s->nb_loc;
	(*loc)->until = now_sec + CURL_KEEP_LOCATION_TIME;
	memcpy((*loc)->location, location, sz_location);
	(*loc)->location[sz_location]= '\0';
	lprintf(LOG_DEBUG, "store_loc: (%p)->%s for URL=%s until=%ld\n"
			 , (*loc), (*loc)->location, s->URL
			 , (*loc)->until - main_start.tv_sec);
	return (*loc)->location;
}


/*
 * @brief does the actual request for the donwload/get_token fallback
 *
 * It is expected to get a 302 (redirect) = HTTP_FOUND. If so the location
 * is returned, otherwise it means the 'redirect' is not supported and
 * the function returns NULL. We use a HEAD req since we want only headers.
 * 
 * @param pointer on the current stream
 * @param pointer on a curl handle
 * @return pointer on the location (or NULL if errors)
 */
static const char *get_loc_fallback_wrapped(struct stream *s, CURL *curl)
{
	CURLcode    res;
	long	    http_code;
	const char *location;

	CURL_EASY_SETOPT(curl, CURLOPT_HTTPHEADER, NULL, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_WRITEFUNCTION, NULL, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_NOBODY, 1L, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_POSTFIELDS, NULL, "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_URL, s->URL, "%s");

	res = curl_easy_perform(curl);

	if (CURLE_OK != res) {
		lprintf(LOG_ERR,
			"%ld on fallback get_token curl_easy_perform url=`%s`.\n",
			res, s->URL);
		return NULL;
	}
	CURL_EASY_GETINFO(curl, CURLINFO_RESPONSE_CODE, &http_code);
	if (HTTP_FOUND != http_code) {
		lprintf(LOG_ERR,
			"expecting http_code 302, got %ld on fallback for url=`%s`.\n",
			http_code, s->URL);
		return NULL;
	}
	CURL_EASY_GETINFO(curl, CURLINFO_REDIRECT_URL, &location);
	return location;
}

/*
 * @brief fallback method to get a download token
 *
 *  If, for whatever reason (except 404 meaning the file is not there), the
 *  get_token API does not work, we try the "good old redirect method".
 *  Since it is automatic and we do not ask for any credentials beside the
 *  API key, this will only work if the "network identification" has been set
 *  and the program is used at the corresponding IP.
 *
 *  The function will test if the "fallback" works, if it does not work (or
 *  at some point stops working), it will never try to use it again, and
 *  the program needs to be restarted if the situation was fixed.
 *
 *  NOTE: when a location is returned, if the path starts with 'p' it is a
 *        'standard' location same as what the API returns with "single":0
 *        or without "single", because 0 is the default.
 *        If the path starts with 's' it is a "single" location, same as
 *        calling the API with "single":1
 *        For a "single", s->loc stays NULL, and the function only duplicates
 *        the string because curl_easy_cleanup will free it. The duplicated
 *        string is used only within stream_fs and will be freed there.
 * 
 *  This function, same as get_new_location is always used under lock, which
 *  means atomics are not needed on the global variables used: fallback_... 
 *
 * @param pointer on the current stream
 * @param address of a pointer on the strm_loc structure
 * @param time (seconds only) when the request started
 * @param pointer on a curl handle
 * @return pointer on the location (or NULL if errors)
 */

static const char *get_loc_fallback(struct stream *s,
				    struct strm_loc **loc,
				    const time_t now_sec,
				    CURL *curl)
{
	const char *location, *p;
	struct timespec	   start;
	bool single = false;

	if (fallback_tested) {
		if (0 == fallback_until)
			return NULL;
	}
	else {
		lprintf(LOG_WARNING,
			"testing fallback to get download tokens.\n");
	}

	/* needed for stats (see comment below) */
	if (NULL != params1f.stat_file)
		clock_gettime(CLOCK_MONOTONIC, &start);

	location = get_loc_fallback_wrapped(s, curl);

	if (NULL == location) {
		if (fallback_tested) {
			lprintf(LOG_ERR,
				"fallback to get download tokens stopped working or network identification changed.\n");
			fallback_until = 0;
		}
		else {
			lprintf(LOG_WARNING,
				"fallback to get download tokens does not work or network identification not used.\n");
			fallback_tested = true;
		}
		return NULL;
	}

	if (!fallback_tested) {
		lprintf(LOG_INFO, "fallback to get download tokens works!\n");
		fallback_tested = true;
	}
	p = strrchr(location, '/');
	if (NULL == p || p[1] != 'p')
		single = true;
		
	/* The common function curl_json_perform also updates the stats, but it
	 * is not used for fallback, so stats must be updated here instead.   */
	if (NULL != params1f.stat_file)
		update_api_stats(FALLBACK_GET_TOKEN,
				 &start,
				 (NULL == location) ? 0 : HTTP_OK,
				 single ? 1 : 0);
	if (single) {
		s->nb_loc ++;
		return fs_strdup(location);
	}
	else {
		return store_loc(s, loc, location, strlen(location), now_sec);
	}
}

/*
 * @brief gets a new location for the URL (or file name if hidden)
 *
 *  This function will use the API donwload/get_token first.
 *  If it does not work, it will revert to the fallback above.
 *  When that happens and fallback is working, to avoid spamming the API
 *  that returned an error, the fallback method is used for KEEP_FALLBACK_TIME
 *  seconds. When that is elapsed, the function tries the API again.
 *
 *  For "hidden" situation, the fallback cannot be used, because there is no
 *  URL to redirect to since precisely the URL are hidden!
 * 
 * @param pointer on the current stream
 * @param address of a pointer on the strm_loc structure
 * @param time (seconds only) when the request started
 * @param pointer on a curl handle
 * @param initialised json structure pointer
 * @param address of a pointer on a curl_slist
 * @return pointer on the location (or NULL if errors)
 */
static const char *get_new_location(struct stream *s,
				    struct strm_loc **loc,
				    time_t now_sec, 
				    CURL *curl,
				    struct json *j,
				    struct curl_slist **list
				   )
{
	long http_code = 0;
	const char *location;
	char *p, *q = NULL;
	static const char down_post[]	= "{\"url\":\"%s\",\"no_ssl\":%d}";
	static const char down_hidden[]	= "{\"filename\":\"%s\",\"sharing_user\":\"%s\",\"folder_id\":%lld,\"no_ssl\":%d}";
	static const char url_pattern[]	= "\"url\":\"";

	if (IS_HIDDEN(s->access)) {
		char buf[sizeof(down_hidden) + strlen(s->URL)
					     + strlen(s->email)
					     + STRLEN_MAX_INT64];
		sprintf(buf, down_hidden, s->URL
					, s->email
					, s->id
					, s->no_ssl);
		http_code = curl_json_perform(curl, list, buf, j,
					      DOWNLOAD_GET_TOKEN, s->URL);
	}
	else
	{
		/* Using directly fallback avoids spamming a not working API. */
		if (now_sec < fallback_until)
			return get_loc_fallback(s, loc, now_sec, curl);
		
		char buf[sizeof(down_post) + strlen(s->URL)];

		sprintf(buf, down_post, s->URL, s->no_ssl);
		http_code = curl_json_perform(curl, list, buf, j,
					      DOWNLOAD_GET_TOKEN, s->URL);
	}
	if (HTTP_OK != http_code) {
		lprintf(LOG_ERR,
			"http_code %lu on get_token json resp.:`%s` for %s\n",
			http_code, j->pb, s->URL);
		if (HTTP_NOT_FOUND == http_code) {
			/* Cache tree out of sync, resync all */
			refresh(&root, REFRESH_404, true);
			return NULL;
		}
		if (IS_HIDDEN(s->access)) /* Can't use fallback on "hidden": */
			return NULL;	  /*     no URL to redirect to!      */

		/* Trying fallback here. If it works keep it for some time.  */
		location = get_loc_fallback(s, loc, now_sec, curl);
		if (NULL != location)
			fallback_until = now_sec + KEEP_FALLBACK_TIME;
		return location;
	}
	p = strstr(j->pb, url_pattern);
	if (NULL != p) {
		p += sizeof(url_pattern) - 1;
		q = strchr(p, '"');
	}
	if (NULL == q)
		lprintf(LOG_CRIT,
			"Unexpected get_token json response:`%s` for %s\n",
			j->pb, s->URL);

	return store_loc(s, loc, p, q - p, now_sec);
}

/*
 * @brief returns a valid location to stream (or NULL if impossible)
 *
 *  This function will first return a valid location for the stream without
 *  locking, if it is the first time it is called from stream_fs (NULL = *loc).
 *  If there is no such valid location, it will lock, and call for a
 *  new location (function above).
 *
 *  Immediately after the lock, it repeats the test because it could
 *  (very unlikely) happen that another thread got the new valid location
 *  while this thread was locked.
 *  This second test is more complex: when the function is called a second
 *  time (see stream_fs) after a location that was supposed valid returned 410
 *  GONE, and the location is valid after the lock, it checks with the i_loc
 *  unique index if it is the same location or a new one.
 * 
 * @param pointer on the current stream
 * @param address of a pointer on the strm_loc structure
 * @return pointer on the location (or NULL if errors)
 */

static const char *get_location(struct stream *s, struct strm_loc **loc)
{
	const char	  *location;
	struct timespec	   now;
	CURL		  *curl;
	struct curl_slist *list = NULL;
	unsigned long	   old_i_loc;
	struct json	   j;

	if (NULL == *loc) {
		*loc = atomic_load_explicit(&s->loc, memory_order_acquire);
		if (NULL != *loc) {
			clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
			if (now.tv_sec < (*loc)->until)
				return (*loc)->location;
		}
		old_i_loc = 0;
	}
	else {
		old_i_loc = (*loc)->i_loc;
	}
	
	lock(&s->curlmutex, NULL);
	/* We have to check again in case another thread got the location
	 * in parallel. 'relaxed' is enough after a lock.
	 */
	*loc = atomic_load_explicit(&s->loc, memory_order_relaxed);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
	if (	(NULL != *loc &&
		 now.tv_sec < (*loc)->until &&
		 old_i_loc != (*loc)->i_loc
		) ||
		 S_ERROR(s)
	   ) {
		location = (*loc)->location;
		lprintf(LOG_DEBUG,
			"Location for %s from another thread: `%s`.\n",
			 s->URL, (*loc)->location);
	}
	else {
		/* RCU-free the s->loc because a new one is requested, also
		 * needed when a 'single' is returned. */
		if (NULL != *loc) {
			struct rcu_free_p *old_loc;

			old_loc = fs_alloc(sizeof(struct rcu_free_p));
			old_loc->p = *loc;
			call_rcu((struct rcu_head *)old_loc, rcu_free_ptr);
			/* Although this section is under lock, we use
			 * release so that other threads get the new value
			 * as soon as possible: that is because we now do
			 * a network operation that can take time.
			 */
			atomic_store_explicit(&s->loc,
					      NULL,
					      memory_order_release);
			*loc = NULL;
		}
		lprintf(LOG_DEBUG,
			"Getting new location for %s %ld\n", s->URL);
		json_init(&j);
		curl = curl_init();
		location = get_new_location(s,
					    loc,
					    now.tv_sec,
					    curl,
					    &j,
					    &list
					   );
		json_free(&j);
		if (NULL != list)
			curl_slist_free_all(list);
		curl_easy_cleanup(curl);
		/* Atomics here are just before an unlock: relaxed is enough */
		if (NULL == location) {
			atomic_fetch_or_explicit(&s->counter,
						 FL_ERROR,
						 memory_order_relaxed);
		}
		else {
			if (NULL == *loc)
				lprintf(LOG_DEBUG,
					"Single location for %s is=`%s`\n",
					s->URL,
					location);
			else
				atomic_store_explicit(&s->loc,
						      *loc,
						      memory_order_relaxed);

		}
	}
	unlock(&s->curlmutex, NULL);
	return location;
}

/*
 * @brief stream (and single range) start
 *
 * Unlike astreamfs, since url details are initialiased with the API.
 * Also the location is taken from the API, which removes the locking/unlocking
 * complexity with the callbacks.
 *
 * It is the only place where curl_easy_perform is used to read files.
 *
 * If the curl on the location returned by get_location fails with 410 Gone (or
 * more infrequently 404), a retry is done because it means the guess on how
 * long to keep the location was wrong.
 *
 * @param pointer on a curl handle
 * @param pointer on the current stream
 * @param curl write callback function to be used
 * @param userdata for the write callback function
 * @return curl_easy_perform return code or STREAM_ERROR
 */

static CURLcode stream_fs(CURL *curl,
			  struct stream *s,
			  curl_write_callback callback,
			  void *userdata)
{
	CURLcode res;
	const char *location;
	struct s_loc sl;
	struct strm_loc *loc = NULL;
	unsigned int iter = 0;

	if (S_ERROR(s))
		return STREAM_ERROR;

	sl.s	= s;
	sl.curl	= curl;
	atomic_fetch_add_explicit(&s->nb_streams, 1L, memory_order_acq_rel);
	do {
		if (1 == iter)
			lprintf(LOG_WARNING,
				"retrying curl_easy_perform (URL=%s) on http_code: %ld\n",
				s->URL, sl.http_code) ;
		/* This is needed to protect s->loc in other threads */
		rcu_register_thread();
		rcu_read_lock();

		location = get_location(s, &loc);

		rcu_read_unlock();
		rcu_unregister_thread();

		if (NULL == location) {
			rcu_read_unlock();
			rcu_unregister_thread();
			return STREAM_ERROR;
		}

		sl.http_code = 0;
		lprintf(LOG_INFO, "curling %s (URL=%s)\n", location, s->URL);
		CURL_EASY_SETOPT(curl, CURLOPT_URL, location, "%s");
		CURL_EASY_SETOPT(curl, CURLOPT_HEADERDATA, &sl, "%p");
		CURL_EASY_SETOPT(curl, CURLOPT_HEADERFUNCTION, header_check, "%p");
		CURL_EASY_SETOPT(curl, CURLOPT_WRITEFUNCTION, callback,"%p");
		CURL_EASY_SETOPT(curl, CURLOPT_WRITEDATA, userdata, "%p");
		res= curl_easy_perform(curl);
		if (NULL == loc)	   /* freeing 'single' location, used */
			fs_free((void *)location); /* only here: no need RCU  */
		iter++;
	} while (iter < 2 && (HTTP_GONE      == sl.http_code ||
			      HTTP_NOT_FOUND == sl.http_code    ));
	
	if (S_ERROR(s))
		return STREAM_ERROR;

	return res;
}


/*
 * @brief asynchronous reader
 *
 * This is the actual asynchronous reader.
 * The very first part initializes the thread local 'work' buffer
 * The main loop waits for some work to arrive from unfichier_read.
 * - if it is a "close" (size is zero) we do nothing and loop back.
 * - otherwise we start reading at the offset requested by unfichier_read, after
 *   having inserted our local cache buffer.
 * We return from reading the stream on 3 conditions:
 * - stream end
 * - close
 * - httfs2_read selected this stream because there were no pending requests
 * In all those cases, we have nothing more to do than loop to the start, the
 * third case will automatically give a new work item and start a stream at
 * the new offset specified.
 *
 * @param opaque pointer
 * @return return value when the thread terminates (never reached for now)
 */

void *
async_reader(void *arg) {
	struct reader *r= arg;
	char range[40];
	struct node *n;
	struct stream *last_s = NULL;
	char buf[KBUF];

	work.buf= buf;
	work.size = 0;

	do {
		if (NULL == r->prv) {
			lprintf(LOG_DEBUG,
				"async_reader[%d] loop: waiting.\n",
				r - readers);

			get_work(r);
			lprintf(LOG_DEBUG,
				"async_reader[%d] received read (%"PRIu64", %lu)\n",
				r - readers, r->prv->offset, r->prv->size );
		}
		if (0 == r->prv->size) {
			n = r->prv;
			r->prv = n->next;
			sem_post(&n->sem_done);
			continue;
		}
		if (S_ERROR(r->s)) {
			struct node *tmp;
			tmp = r->prv->next;
			r->prv->written = -EIO;
			if (&work != r->prv)
				sem_post(&r->prv->sem_done);
			r->prv = tmp;
			r->idle = true;
			continue;
		}
		if (last_s != r->s) {
			if (NULL != r->curl)
				curl_easy_cleanup(r->curl);
			r->curl = curl_init(r->s);
			last_s = r->s;
		}
		if (!r->streaming) {
			/* Add private buffer on top if necessary */
			if (r->prv->offset > r->start) {
				work.offset = r->start;
				work.written = 0;
				work.size = r->prv->offset - r->start;
				work.next = r->prv;
				r->prv = &work;
			}
			r->pos = r->start;
			r->streaming = true;
			sprintf(range,"%"PRIu64"-", r->start);
		}
		else {
			sprintf(range,"%"PRIu64"-", r->pos);
		}
		CURL_EASY_SETOPT(r->curl, CURLOPT_RANGE, range, "%s");
		lprintf(LOG_DEBUG, "async_reader[%d] range is %s\n",
			r - readers, range);

		/* Because streams are not closed on release, this is to make
		 * sure the stream won't be freed as long as it is live
		 */
		atomic_fetch_add_explicit(&r->s->counter, OPEN_INC,
					  memory_order_acq_rel);
		if (CURLE_OK != stream_fs(r->curl,
					  r->s,
					  (curl_read_callback)write_data,
					  r)
			&&
		    r->streaming) {
			lprintf(LOG_DEBUG, "stream_fs exception[%d].\n"
					 , r - readers);
			lock(&r->s->fastmutex, NULL);
			for (n = r->prv; n != NULL; n = n->next) {
				if (&work == n) {
					n->written = 0;
				}
				else {
					n->written = -EIO;
					sem_post(&n->sem_done);
				}
			}
			n = atomic_load_explicit(&r->rcv, memory_order_relaxed);
			while (NULL != n) {
				int err;
				err= sem_wait(&r->sem_go);
				if ( 0 != err )
					lprintf(LOG_CRIT, "async_reader[%d] sem_wait error: %d.\n",
						r - readers, err);
				n->written = -EIO;
				sem_post(&n->sem_done);
				n = n->next;
			}
			r->idle = true;
			r->prv = NULL;
			atomic_store_explicit(&r->rcv, NULL,
					      memory_order_relaxed);
			unlock(&r->s->fastmutex, NULL);
		}
		atomic_fetch_sub_explicit(&last_s->counter, OPEN_INC,
					  memory_order_acq_rel);
	} while (!atomic_load_explicit(&exiting, memory_order_acquire));

	return NULL;
}


static void readers_dump(struct reader *r_dup, unsigned long long this_rq_id,
			 int i)
{
	int j;
	for (j=0; j< MAX_READERS; j++)
		lprintf(LOG_DEBUG,
			"dump: readers[%"PRIu64"%c%d] fs=%p %"PRIu64":%"PRIu64" (idle=%d) (n_rq=%u)\n",
			this_rq_id, (i==j)?'*':'|', j, r_dup[j].s,
			r_dup[j].start,	r_dup[j].end, r_dup[j].idle,
			r_dup[j].n_rq);

}

/* Global read counter, especially useful for debug */
_Atomic unsigned long long read_rq_id = 0; 

/*
 * @brief fuse callback for read
 *
 * This function is called each time there is a block of data to read.
 * Steps:
 * - encapsulates the read request in our thread local 'work' buffer, including
 *   an arrival timestamp
 * - under lock, selects the relevant async reader for that read block.
 *   Each reader exposes a "read slice": start-end, a pending number of requests
 *   and a flag for idle state.
 *   -1) Best match is a block inside our "read slice" or at most 128k beyond
 *	 the end (128k being the kernel's readahead cache buffer size)
 *   -2) Second the "oldest" idle reader
 *   -3) Last the "oldest" reader with no pending requests
 * - Match 1) uses an existing stream reader.
 * - Match 2 and 3) will start a new stream, either on an already idle reader
 *	 or terminating the waiting stream plus start a new one.
 *	 We always add a 128K buffer ahead of the offset we received to better
 *	 serve out of order requests.
 *  - Match 4) is when no match is found: we do a range request with the exact
 *	 size instead of giving the request to the async reader.
 * - Once that is done, we put the request on the selected reader's receive
 *   queue and signal it with a semaphore (unless case 4).
 * - Now we only need to wait on the "done" semaphore for the reader to fill
 *   our buffer.
 * - We can then display the time it took and return to fuse.
 *
 * @param path to the file being read
 * @param buffer where we store read data
 * @param size of the buffer
 * @param offset from where we read
 * @param fuse internal pointer with possible private data
 * @return number of bytes read (should be size unless error or end of file)
 */
static int unfichier_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	struct timespec start;
	int i, i_idle, i_wait;
	unsigned long last_rq_idle, last_rq_wait, this_rq_id;
	struct stream *s;
	struct reader r_dup[MAX_READERS];

	if (unlikely(NULL == work.buf))
		sem_init(&work.sem_done, 0, 0);

	s = (struct stream *)((uintptr_t)(fi->fh));

	/* This is the constant address we have for refresh */
	if (s == (struct stream *)params1f.refresh_file)
		return 0;

	/* We don't need atomic here because that happens only for stats
	 * where the counter need not be atomic: does not change after malloc */
	if (0 != (s->counter & FL_SPECIAL)) {
		if (offset + size >= ((struct stats *)s)->size) {
			if (offset >= ((struct stats *)s)->size)
				return 0;
			else
				size = ((struct stats *)s)->size - offset;
		}
		memcpy(buf, ((struct stats *)s)->buf + offset, size);
		return size;
	}

	/* Shortcut in case of impossible read: beyond file end! */
	if (offset >= s->size) {
		lprintf(LOG_DEBUG,
			">>*<< impossible read beyond EOF (fast return 0): (%"PRIu64",%lu).\n",
			offset,	size);
		return 0;
	}

	/* Note: _COARSE clock does not have enough resolution here */
	if (NULL != params1f.stat_file)
		clock_gettime(CLOCK_MONOTONIC, &start);

	this_rq_id = atomic_fetch_add_explicit(&read_rq_id, 1ULL,
					       memory_order_acq_rel);
	lprintf(LOG_DEBUG, ">> unfichier_read: %p (%"PRIu64",%lu) rq[%d]--------\n"
			 , s, offset, size, this_rq_id);
	
	work.buf     = buf;
	work.offset  = offset;
	work.written = 0;
	if (offset + size > s->size)
		work.size = s->size - offset;
	else
		work.size = size;

	i_idle = -1;
	i_wait = -1;
	last_rq_idle = ULONG_MAX;
	last_rq_wait = ULONG_MAX;

	/*
	 * Start critical section
	 */
	lock(&s->fastmutex, NULL);
	if (LOG_DEBUG == params.log_level)
		memcpy(r_dup, readers, sizeof(readers));
	for (i=0; i < MAX_READERS; i++) {
		if (readers[i].idle) {
			if (readers[i].last_rq_id < last_rq_idle) {
				i_idle = i;
				last_rq_idle = readers[i].last_rq_id;
			}
		}
		else {
			if (readers[i].s == s &&
			    work.offset >= readers[i].start &&
			    work.offset <= readers[i].end + KBUF)
			    break;
			if (0 == readers[i].n_rq &&
			    readers[i].last_rq_id < last_rq_wait) {
				i_wait = i;
				last_rq_wait = readers[i].last_rq_id;
			}
		}
	}
	if (i == MAX_READERS) {
		i = (-1 == i_idle) ? i_wait : i_idle;
		if (i == -1) {
			char range[40];
			CURL *curl;
			
			s->last_rq_id = this_rq_id;
			unlock(&s->fastmutex, NULL);

			lprintf(LOG_DEBUG,
				"no reader available rq[%"PRIu64"], doing single range: %"PRIu64"-%"PRIu64"\n",
				this_rq_id, offset, offset + size - 1);
			readers_dump(r_dup, this_rq_id, i);
			curl = curl_init(s);
			sprintf(range,"%"PRIu64"-%"PRIu64, offset
							 , offset + size - 1);
			CURL_EASY_SETOPT(curl, CURLOPT_RANGE, range, "%s");
			if (0 != stream_fs(curl, s,
					   (curl_read_callback)single_range,
					   &work))
				work.written = -EIO;
			curl_easy_cleanup(curl);
			lprintf(LOG_INFO,
				">> read-response[single]: (%"PRIu64",%lu|%d)\n",
				this_rq_id, offset, size, work.written);

			if (NULL != params1f.stat_file)
				update_single_stats(work.written, &start);

			return (int)work.written;
		}
		readers[i].start = (work.offset > KBUF) ?
				    work.offset - KBUF : 0;
		readers[i].end = work.offset + work.size;
/* Infinite loop bug if reader's end goes beyond the received buffer
		if (readers[i].end < KBUF)
			readers[i].end = KBUF;
 * This test is useless already done above 
		if (readers[i].end > s->size)
			readers[i].end = s->size;
*/
		readers[i].idle = false;
	}
	else {
		if (offset + size > readers[i].end)
			readers[i].end = offset + size;
	}
	atomic_fetch_add_explicit(&readers[i].n_rq, 1, memory_order_acq_rel);
	s->last_rq_id 	      =
	readers[i].last_rq_id = this_rq_id;
	readers[i].s = s;
	msg_push(&readers[i].rcv, &work);
	sem_post(&readers[i].sem_go);
	unlock(&s->fastmutex, NULL);
	/*
	 * End critical section
	 */
	readers_dump(r_dup, this_rq_id, i);
	sem_wait(&work.sem_done);
	atomic_fetch_sub_explicit(&readers[i].n_rq, 1, memory_order_acq_rel);

	lprintf(LOG_INFO,
		">> read-response[%u]: (%"PRIu64",%lu|%d)\n",
		this_rq_id, offset, size, work.written);

	if (NULL != params1f.stat_file)
		update_read_stats(work.written, i, &start);

	return work.written;
}

void unfichier_destroy(void * priv_data)
{
	unsigned int i;
	void * ret;
	lprintf(LOG_INFO, "Entering destroy.\n");

	atomic_store_explicit(&exiting, true, memory_order_release);
	work.size = 0;
	if (NULL == work.buf)
		sem_init(&work.sem_done, 0, 0);
	for (i=0; i< MAX_READERS; i++) {
		msg_push(&readers[i].rcv, &work);
		sem_post(&readers[i].sem_go);
		sem_wait(&work.sem_done);
		pthread_join(readers[i].thread, &ret);
		sem_destroy(&readers[i].sem_go);
		lprintf(LOG_INFO, "<< thread reader[%d] ended.\n", i);
	}
	sem_destroy(&work.sem_done);
}

struct fuse_operations unfichier_oper = {
	.init		= astreamfs_init,
	.destroy	= unfichier_destroy,
	.getattr	= unfichier_getattr,
	.readdir	= unfichier_readdir,
	.open		= unfichier_open,
	.read		= unfichier_read,
	.release	= unfichier_release,
	.statfs		= unfichier_statfs,
	.rename		= unfichier_rename,
	.unlink		= unfichier_unlink,
	.rmdir		= unfichier_rmdir,
	.link		= unfichier_link,
	.mkdir		= unfichier_mkdir,
};

/* Note: this long_names list must be alphabetically sorted */
extern const char *long_names[];
const unsigned int s_long_names;
static const char end_expand[] = "Alo";


void cleanup(struct fuse_args *args)
{
	unsigned int i;
	struct streams *cur;
	
	lprintf(LOG_NOTICE,"Cleaning upon fuse unmount.\n");
	
	curl_global_cleanup();
	fuse_opt_free_args(args);

	if (def_user_agent != params.user_agent)
		fs_free(params.user_agent);
	if (def_type != params.filesystem_subtype)
		fs_free(params.filesystem_subtype);
	if (def_fsname != params.filesystem_name)
		fs_free(params.filesystem_name);
	if (def_root != root.name)
		fs_free(root.name);
	for (i=0; i< MAX_READERS; i++)
		if (NULL != readers[i].curl)
			curl_easy_cleanup(readers[i].curl);
	fs_free(params1f.a_no_ssl);
	if (NULL != root.dent)
		refresh(&root, REFRESH_EXIT, false);
	rcu_free_queue(&rcu_defered);
	cur = atomic_load_explicit(&live, memory_order_relaxed);
	if (NULL != cur) {
		for (i = 0; i < cur->n_streams; i++)
			free_stream(cur->a_streams[i],
				    atomic_load_explicit(
						&cur->a_streams[i]->counter,
						memory_order_relaxed));
		rcu_free_queue(&rcu_defered);
		fs_free(cur);
	}
	rcu_exit();
	atomic_store_explicit(&live, NULL, memory_order_release);
	fs_free(params1f.refresh_file);
	fs_free(params1f.stat_file);
	fs_free(params.ca_file);
	fs_free(params1f.api_key);
	if (NULL != params.log_file) {
		fs_free(params.log_file);
		if (NULL != params1f.stat_file) {
			struct stats end_stats;

			out_stats(&end_stats);
			fputs(end_stats.buf, log_fh);
		}
		fclose(log_fh);
	}
}

/******************************
 */
int main(int argc, char *argv[])
{
	unsigned int ret = 0;
	struct parse_data data = { false, NULL, NULL, 0UL };
	struct fuse_args exp_args = {argc, argv, false};
	struct fuse_args args;
	struct timespec t;

	clock_gettime(CLOCK_REALTIME, &t);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &main_start);

	expand_args(&exp_args, long_names, s_long_names, end_expand);

	/* First pass = counting (for memory allocation) + help, version, etc */
	args.argc = exp_args.argc;
	args.argv = exp_args.argv;
	args.allocated = false;
	if (0 != fuse_opt_parse(&args,
				&data,
				unfichier_opts,
				unfichier_opt_count))
		lprintf(LOG_ERR, "parsing arguments.\n");
	fuse_opt_free_args(&args);

	check_args_first_pass(&data);


	/* Second pass = parse all arguments + populate allocated memory      */
	args.argc = exp_args.argc;
	args.argv = exp_args.argv;
	if (0 != fuse_opt_parse(&args,
				&data,
				unfichier_opts,
				unfichier_opt_proc))
		lprintf(LOG_ERR, "parsing arguments.\n");


	if (exp_args.allocated)
		fs_free(exp_args.argv);

	/* Check arguments, and output debug information */
	check_args_second_pass(&t);

	if (CURLE_OK != curl_global_init_mem(CURL_GLOBAL_ALL,
					     fs_alloc,
					     fs_free,
					     fs_realloc,
					     fs_strdup,
					     fs_calloc))
		lprintf( LOG_ERR, "initializing curl: curl_global_init().\n" );

	rcu_init();
	if (NULL == data.root)
	{
	/* Initialize root now to check the API Key, so that if it fails
	 * the program exits before starting the fuse daemon.
	 */
		struct dentries *dent;

		init_dir(&root, &dent);
	}
	else
	{
	/* When we are mounting only a part of the account we already do some
	 * requests to test the specified path, no need to add an init_dir.
	 */
		struct walk w;
		long long root_id;
		unsigned long root_access;
		char *root_name;

		find_path(data.root, &w);
		if (NULL == w.res.dir)
				lprintf( LOG_ERR, "root path: `%s` does not exist.\n"
					, data.root);
		if (!w.is_dir)
			lprintf( LOG_ERR, "root path: `%s` is not a directory.\n"
					, data.root);
		lprintf(LOG_NOTICE,"Mounting only the remote path: %s\n"
				  , data.root);
		root_id     = w.res.dir->id;
		root_access = w.res.dir->access;
		if (IS_SHARED_ROOT(w.res.dir))
			root_name = fs_strdup(w.res.dir->name);
		else
			root_name = NULL;
		refresh(&root, REFRESH_INIT_ROOT, false);
		root.id     = root_id;
		root.access = root_access;
		if (NULL != root_name)
			root.name = root_name;
		fs_free(data.root);
		if (ACCESS_RW != (root_access & ACCESS_MASK))
			params1f.readonly = true;
	}

	check_args_third_pass(&args);

	/* Starting the fuse mount now!
	*/
	lprintf(LOG_NOTICE,"Init done, starting fuse mount.\n");
	init_done= true;
	{
		int i;
		for (i = 0; i < args.argc; i++)
			lprintf(LOG_DEBUG, "arg[%d]: %s\n", i, args.argv[i]);
	}
	ret = fuse_main(args.argc,
			args.argv,
			&unfichier_oper,
			NULL);


	/* Cleaning */
	cleanup(&args);

	return ret;
}
